/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/17/2016
* Description     : Is called when intellegence is successfully gathered.
* **LOCALIZED**
*/

//TODO Re-write params based on _source switch - cuz they are named weird between the two.

private ["_source","_enemyGrp","_nearEnemy","_DS_ENEMY_SIDE","_item","_target"]; 
_source   = (_this select 3) select 0;
_enemyGrp = (_this select 3) select 1;
_item     = (_this select 3) select 2;
_four     = (_this select 3) select 3;
_target   = _this select 0;
_bldgPos  = if (count (_this select 3) > 4) then { (_this select 3) select 4};

switch (_source) do {
  case "Infantry": {
    (_target)  remoteExecCall ["removeAllActions",[0,2] select isDedicated];
    if (random 100 <= DS_INTEL_INF_SIDEMISSION) exitWith {
      [] spawn DS_fnc_selectRandomsideMission;
      [profileName, localize "str_DS_intel_INF_SIDE"] remoteExec ["BIS_fnc_showSubtitle",group player];
    };
    if (random 100 <= DS_INTEL_INF_NEXT_ENEMY_REVEAL) exitWith {
      //Reveal Next Nearby Squad to the player's group!
      _nearEnemy = nearestObjects [player, ["Car","Tank","Man"], 1000];
      { 
        if (count _nearEnemy == 0 ) exitWith {
          [profileName, localize "str_DS_intel_INF_WIPEOUT"] remoteExec ["BIS_fnc_showSubtitle",group player];
        };
        if (side _x == DS_ENEMY_SIDE) exitWith { 
          {
            (group player) reveal [_x,4];
          } forEach units group (_x);
          [profileName, localize "str_DS_intel_INF_NEARBY"] remoteExec ["BIS_fnc_showSubtitle",group player]; 
        };
      } forEach _nearEnemy;
    };
      //If we've got this far - just give them the locations of his squadmates.
      
      private ["_c"]; _c = 0;
      {
        if (alive _x) then {_c = _c + 1};
      } foreach units _enemyGrp;

      if (_c == 0 ) then {
        //Noone in his group is alive - terrible - intel does nothing.
        [profileName, localize "str_DS_intel_INF_WIPEOUT"] remoteExec ["BIS_fnc_showSubtitle",group player];
      } else {
      { (group player) reveal [_x,4];} forEach units _enemyGrp;
      [profileName, localize "str_DS_intel_INF_GROUP"] remoteExec ["BIS_fnc_showSubtitle",group player];
      };
      deleteVehicle _target;
  };
  case "Cache"   : {
    (_target)  remoteExecCall ["removeAllActions",[0,2] select isDedicated];
    if (random 100 <= DS_INTEL_CACHE_SIDEMISSION) exitWith {
      [profileName, localize "str_DS_intel_CACHE_SIDE"] remoteExec ["BIS_fnc_showSubtitle",group player];
      [] spawn DS_fnc_selectRandomsideMission;
    };
      //If we've got this far - let them know of all remaining enemies within 3.0km
      _nearEnemy = nearestObjects [player, ["Car","Tank","Man"], 3000];
      { 
        if (side _x == DS_ENEMY_SIDE) then { 
          { 
            (group player) reveal [_x,4];
          } forEach units group (_x); 
        };
      } forEach _nearEnemy;
      [profileName, localize "str_DS_intel_CACHE_NEARBY"] remoteExec ["BIS_fnc_showSubtitle",group player];
      hint format["GatherScript: Comp: %1",_item];
      [_item] spawn LARs_fnc_deleteComp;
      DS_OPCOM_MARKERS_USED = DS_OPCOM_MARKERS_USED - [_four]; publicVariable "DS_OPCOM_MARKERS_USED";
  };
};