/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/13/2016
* Description     : Spawns a random intel cache composistion and then sets it up for 'capturing'.

// Alive markers start with ALiVE_OPCOM_ - from there we can find enterable building positions.
// DS_INTELCACHE_LOCS_USED are blacklisted locations already in use! (building positions)

// if !(<where i wanna spawn> in DS_INTELCACHE_LOCS_USED) then { valid location };
*/
private ["_curOpcomMarkers"]; _curOpcomMarkers = [];

{
  private ["_mrk"];
  _mrk = toArray _x;
  _mrk resize 12;
  if ((toString _mrk) == "ALiVE_OPCOM_") then { _curOpcomMarkers pushback _x; };
} forEach allMapMarkers;

private ["_selectedMarker"]; _selectedMarker = "";

private ["_countOpComsUsed"]; _countOpComsUsed = count DS_OPCOM_MARKERS_USED;

if (_countOpComsUsed < count _curOpcomMarkers) then {
  _selectedMarker = _curOpcomMarkers call bis_fnc_selectRandom;
  while {_selectedMarker in DS_OPCOM_MARKERS_USED} do {
    _selectedMarker = _curOpcomMarkers call bis_fnc_selectRandom;
  };

  DS_OPCOM_MARKERS_USED pushBack _selectedMarker; publicVariable "DS_OPCOM_MARKERS_USED";

  private ["_buildings","_bldg","_bldgPos"];
  _buildings = [(getMarkerPos _selectedMarker), 1000] call DS_fnc_getEnterableBldgs;

  //--- Check we can complete the function - if not loop back around.
  if (isNil "_buildings")    exitWith { [] spawn DS_fnc_spawnCache; };
  if (count _buildings == 0) exitWith { [] spawn DS_fnc_spawnCache; };

  //--- Select a random enterable house
  _bldg = _buildings call bis_fnc_selectRandom;

  //--- Finding random indoor position for spawning the cache
  _bldgPos = [_bldg] call DS_fnc_findRandBldgPos; 
  while {_bldgPos in DS_BLDGPOS_USED} do {
    _bldgPos = [_bldg] call DS_fnc_findRandBldgPos; 
  };

  //--- We have our prospective building position now - now we get to check if it is still red - if so , we can spawn
  private ["_domMarkers"]; _domMarkers = [] call DS_fnc_returnDomMarkers;
  private ["_domMarkersRed"]; _domMarkersRed = _domMarkers select 2;
  hint format["%1",_bldgPos];
  //player setpos _bldgpos; //DEBUG value
  {
    if ([_x,_bldgPos] call BIS_fnc_InTrigger) exitWith {
      // Ding Ding Ding - the square is red for the spawn location - go ahead and spawn.
      private ["_compReference","_bldgPos2"];
      _bldgPos2 = [_bldgPos select 0,_bldgPos select 1,0];
      _compReference = ["intelcachetable1",_bldgPos2] call LARs_fnc_spawnComp;
      hint _compReference;
      private ["_list"];
      _list = _bldgPos nearObjects ["Land_WoodenTable_large_F",20];
      private ["_rootObj"]; _rootObj = _list select 0;
      [_rootObj,                                                       // Object the action is attached to
      localize "str_DS_intel_GatherIntel",                                 // Title of the action  (TODO: USE STRINGTABLE)
      "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",       // Idle icon shown on screen
      "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",       // Progress icon shown on screen
      "_this distance _target < 5",                                        // Condition for the action to be shown
      "_caller distance _target < 3",                                      // Condition for the action to progress
      {},                                                                  // Code executed when action starts
      {},                                                                  // Code executed on every progress tick
      {
        _this call DS_fnc_intelGatherComplete;
        [(_this select 0),(_this select 2)] remoteExec ["bis_fnc_holdActionRemove",[0,-2] select isDedicated,_this select 0];
      },                           // Code executed on completion
      {},                                                                  // Code executed on interrupted
      ["Cache",_rootObj,_compReference,_selectedMarker,_bldgpos],                   // Arguments passed to the scripts as _this select 3
      10,                                                                  // Action duration [s]
      0,                                                                   // Priority
      true,                                                                // Remove on completion
      false                                                                // Show in unconscious state 
      ] remoteExecCall ["BIS_fnc_holdActionAdd",[0,-2] select isDedicated,_rootObj];  // example for MP compatible implementation

if (DS_DEBUG) then {	[_bldgPos,format["debug%1",random 1000],1,"hd_dot","ColorYellow",true,"INTEL"] call DS_fnc_AddMarker; };
  DS_BLDGPOS_USED pushBack _bldgPos; publicVariable "DS_BLDGPOS_USED";    
    
    };
} forEach _domMarkersRed;

};