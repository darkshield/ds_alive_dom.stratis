/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/11/2016
* Description     : Is called via Man Eventhandler when unit is killed.
* **LOCALIZED**
*/
if (DS_SRV || DS_DEDICATED) then {
  private["_unit","_selecteditem","_droppedItem","_dropChance"];
  _unit = _this select 0;
  if (DS_DEBUG) then {_dropChance = 100} else { _dropChance = DS_PARAM_INTELDROPCHANCE};
  if (random 100 <= _dropChance) then {
    _selecteditem = DS_INTELDROP_CLASSES call BIS_fnc_selectrandom;
    _droppedItem = createVehicle [_selecteditem,getPos _unit,[],0,"None"];
    [_droppedItem,                                                       // Object the action is attached to
    localize "str_DS_intel_GatherIntel",                                 // Title of the action  (TODO: USE STRINGTABLE)
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",       // Idle icon shown on screen
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",       // Progress icon shown on screen
    "_this distance _target < 5",                                        // Condition for the action to be shown
    "_caller distance _target < 3",                                      // Condition for the action to progress
    {},                                                                  // Code executed when action starts
    {},                                                                  // Code executed on every progress tick
    {_this call DS_fnc_intelGatherComplete},                             // Code executed on completion
    {},                                                                  // Code executed on interrupted
    ["Infantry",group _unit,_droppedItem],                               // Arguments passed to the scripts as _this select 3
    10,                                                                  // Action duration [s]
    0,                                                                   // Priority
    true,                                                                // Remove on completion
    false                                                                // Show in unconscious state 
    ] remoteExecCall ["BIS_fnc_holdActionAdd",[0,-2] select isDedicated,_droppedItem];  // example for MP compatible implementation
    _droppedItem setPos (_droppedItem modelToWorld [1.2,1.4,0]);
  };
};
