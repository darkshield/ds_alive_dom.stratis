/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/12/2016
* Description     : Creates a marker with circular radius that contains the location somewhere randomly within.
*
* Arguments:
* 1: _pos       - [REQUIRED]
* 2: _taskName  - [OPTIONAL] STRING - Prefix of the marker name to be found later.
* 3: _radius    - [OPTIONAL] NUMBER - Radius for Marker's Circle - Default: 100
* 4: _type      - [OPTIONAL] STRING - "ELLIPSE" or "DOT" - Default: "ELLIPSE"
* 5: _color     - [OPTIONAL] STRING - "GREEN" - Default: "RED"
* 6: _debug     - [OPTIONAL] BOOL   - Is this is a debug marker? Default: false
* 7: _debugtext - [OPTIONAL] STRING - Text to show next to icon - Default: "Debug"
*
* Return: <MARKER> - marker created
* 
* USAGE: [_position,500,"ELLIPSE","GREEN","TASK12",true] call DS_fnc_AddMarker; /adds green ELLIPSE 500rad marker to debug named "TASK12_5831"
* USAGE: [_position] call DS_fnc_AddMarker;  //Creates a red ELLIPSE with radius 100
*/
params  ["_pos",
        ["_taskName","marker",[""]],
        ["_rad",100],
        ["_type","ELLIPSE",   [""]],
        ["_col","ColorGreen", [""]],
        ["_debug",false],
        ["_debugtext","Debug",[""]]];

private ["_newMarker"];

if (_debug) then {
  _newMarker = createMarker [format ["%1:%2_DEBUG",_taskName, random 10000], _pos];
  _newMarker setMarkerType _type;
  _newMarker setMarkerText _debugtext;
} else {
  _newMarker = createMarker [format ["%1:%2",_taskName,random 1000], _pos];
  _newMarker setMarkerBrush "BORDER";
  _newMarker setMarkerShape _type;
};

  _newMarker setMarkerColor _col;

if ( markerShape _newMarker == "ELLIPSE" ) then {
  _newMarker setMarkerSize [_rad,_rad];
};

_newMarker