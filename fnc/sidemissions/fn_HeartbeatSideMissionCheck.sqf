/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : BBrown
* Last Editor     : BBrown
* Last Edited     : 09/17/2016
* Description     : Checks every 'x' seconds if there is an active side mission, if not then it has a percentage chance to generate one 
* **LOCALIZED**
*/
if !(DS_SRV || DS_DEDICATED) exitWith {};

if(DS_DEBUG) then {
  ["fn_HeartbeatSideMissionCheck.sqf | Starting script..."] call ALiVE_fnc_Dump;
};
private ["_sleepTime","_missionSpawnChance"];

_sleepTime = 60;  //TODO: change to higher value
_missionSpawnChance = DS_PARAM_ONESIDEMISSIONCHANCE; //30% chance to spawn side mission

sleep _sleepTime;
//run intel cache script
[] spawn DS_fnc_spawnCache;

//check if there are any active side missions
if(DS_SIDEMISSIONS_ACTIVE_COUNT > 0) then {
  if(DS_DEBUG) then {hint format["Side mission currently active. Sleeping for %1 seconds",_sleepTime];};
  //run the heartbeat script
  [] spawn DS_fnc_HeartbeatSideMissionCheck;
} else {
  if((random 100) < _missionSpawnChance) then {
    if(DS_DEBUG) then {hint "Conditions have been met, generating side mission";};

    //spawn side mission
    [] spawn DS_fnc_selectRandomsideMission;
    sleep 1;
    //rinse and repeat
    [] spawn DS_fnc_HeartbeatSideMissionCheck;
  } else {
    hint format["Conditions not met for side mission, sleeping for %1 seconds.",_sleepTime];
    [] spawn DS_fnc_HeartbeatSideMissionCheck;
  };
};

if(DS_DEBUG) then {
  ["fn_HeartbeatSideMissionCheck.sqf | Finishing script..."] call ALiVE_fnc_Dump;
}