/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/12/2016
* Description     : Handles cleaning up the task - and making sure the marker is deleted.
* **LOCALIZED**
*/ 

private ["_cache","_pos","_endExplosions","_markerName","_tname","_bomber"];
_cache      = _this select 0;
_markerName = _this select 1;
_tname      = _this select 2;
_bomber     = _this select 3;

_pos = getPos _cache;

//--- Create explosion effect
_endExplosions = [_pos,_markerName,_tname,_cache,_bomber] spawn {
  [name _bomber, localize "str_DS_task_killWepCache"] remoteExec ["BIS_fnc_showSubtitle",group player];
  private ["_x","_id"]; _x = 0;
  while {_x <= 22} do {
    "M_Mo_82mm_AT_LG" createVehicle (_this select 0);
    _x = _x + 1 + random 3;
    sleep (0.2 + (random 3));
  };
  deletevehicle (_this select 3);
  //------------------------------ ** END OF TASK CLEANUP--------------------------------------------------------------
    if(DS_SIDEMISSIONS_ACTIVE_COUNT == 0) then {
    DS_SIDEMISSIONS_ACTIVE_COUNT = 0; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";
  } else {
    DS_SIDEMISSIONS_ACTIVE_COUNT = DS_SIDEMISSIONS_ACTIVE_COUNT - 1; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";
  };
  [_this select 1,"EAST"] remoteExecCall ["DS_fnc_aliveremoveobjectivefromsides",2];
  (_this select 1) remoteExecCall ["deleteMarker",[0,2] select isDedicated];
  [(_this select 2), "Succeeded"] remoteExecCall ["BIS_fnc_taskSetState",[0,-2] select isDedicated];
  [] spawn DS_fnc_SendReward;
  //------------------------------ ** END OF TASK CLEANUP--------------------------------------------------------------
};
