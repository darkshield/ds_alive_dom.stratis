/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : BBrown
* Last Editor     : BBrown
* Last Edited     : 09/16/2016
* Description     : Handles spawning a hostile convoy 
* **LOCALIZED**
*/

private ["_convoyVehs","_cnvGrp","_gap","_convoyWP"];

_convoyVehs = ["O_MRAP_02_hmg_F","O_Truck_03_ammo_F","O_Truck_03_fuel_F","O_Truck_03_repair_F","O_MRAP_02_hmg_F"];

DS_CONVOY_VEHICLES = [];
DS_CONVOY_VEHICLECOUNT = 0;
DS_CONVOY_ARRIVED = false;

private ["_mrkStart","_mrkEnd","_taskName"];
_taskName = format["TASK%1",random 1000];
_title = format[localize "str_DS_task_destroyConvoyTitle"];
_brief = format[localize "str_DS_task_destroyConvoyBrief"];
[west,[_taskName],[_brief,_title,""],objNull,"CREATED",1,true,"Destroy"] call bis_fnc_taskcreate;
DS_SIDEMISSIONS_ACTIVE_COUNT = DS_SIDEMISSIONS_ACTIVE_COUNT + 1; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";

_mrkStart = createMarker ["convoy_start",getMarkerPos "convoy_sidemission_start"];
_mrkStart setMarkerType "hd_start";
_mrkStart setMarkerColor "ColorRed";
_mrkStart setMarkerText "Convoy Start";

_mrkEnd = createMarker ["convoy_end",getMarkerPos "convoy_sidemission_end"];
_mrkEnd setMarkerType "hd_end";
_mrkEnd setMarkerColor "ColorRed";
_mrkEnd setMarkerText "Convoy End";

_cnvGrp = createGroup EAST;
_gap = 0;
_newPos = getMarkerPos "convoy_sidemission_start";

{
    // Current result is saved in variable _x
    _veh = [_newPos,0,_x,_cnvGrp] call BIS_fnc_spawnVehicle;
    (_veh select 0) setDir markerDir "convoy_sidemission_start";
    (_veh select 0) setPosATL [getPosATL (_veh select 0) select 0, getPosATL (_veh select 0) select 1, 0];
    _gap = _gap - 10;
    _newPos = (_veh select 0) modelToWorld [0,_gap,0];

    DS_CONVOY_VEHICLES pushback (_veh select 0);
    (_veh select 0) addEventHandler ["killed", {DS_CONVOY_VEHICLECOUNT = DS_CONVOY_VEHICLECOUNT - 1; hint format["%1",DS_CONVOY_VEHICLECOUNT]}];
    sleep 0.25;
} forEach _convoyVehs;

_convoyWP = _cnvGrp addWaypoint [getMarkerPos "convoy_sidemission_end",0];
_convoyWP setWaypointType "MOVE";
_convoyWP setWaypointBehaviour "SAFE";
_convoyWP setWaypointSpeed "NORMAL";
_convoyWP setWaypointFormation "COLUMN";
_convoyWP setWaypointStatements ["true","{deleteVehicle _x} foreach thislist;{deleteVehicle _x} foreach DS_CONVOY_VEHICLES; DS_CONVOY_ARRIVED = true;"];

DS_CONVOY_VEHICLECOUNT = count DS_CONVOY_VEHICLES;

waitUntil { DS_CONVOY_VEHICLECOUNT == 0 || DS_CONVOY_ARRIVED};

if(DS_CONVOY_ARRIVED) then {
    //fail sidemission and call cleanup
    [_taskName,"FAILED","convoy_sidemission_start","convoy_sidemission_end"] spawn DS_fnc_sm_end_convoy;
} else {
    //complete sidemission and call cleanup
    [_taskName,"SUCCEEDED","convoy_sidemission_start","convoy_sidemission_end"] spawn DS_fnc_sm_end_convoy;
}