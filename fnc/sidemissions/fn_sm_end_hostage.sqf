/*
* **LOCALIZED**
*/
private["_host","_taskName","_mrkName","_alive"];
_host     = (_this select 3) select 0;
_taskName = (_this select 3) select 1;
_mrkName  = (_this select 3) select 2;
_alive    = (_this select 3) select 3;


switch (_alive) do {
  case "Dead": {
    [profileName, localize "str_DS_task_hostageDeadReport"] remoteExec ["BIS_fnc_showSubtitle",group player];
    sleep 10;
    [_taskName, "Failed"] remoteExecCall ["BIS_fnc_taskSetState",[0,-2] select isDedicated];
    [_mrkName,"EAST"]  remoteExecCall ["DS_fnc_aliveremoveobjectivefromsides",2];
    _mrkName remoteExecCall ["deleteMarker",[0,2] select isDedicated];
    ["HeadQuarters", localize "str_DS_task_hostageFailed"] remoteExec ["BIS_fnc_showSubtitle",[0,-2] select isDedicated]; 
    DS_SIDEMISSIONS_ACTIVE_COUNT = DS_SIDEMISSIONS_ACTIVE_COUNT - 1; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT"; 
  };
  case "Alive": {
    // Join the group! - do nothing else!
    _host setCaptive false;
    //_host disableAI "AUTOTARGET";
    _host enableAI "MOVE";
    [_host] joinSilent (group player);
  };
  case "Returned": {
    [_taskName, "Succeeded"] remoteExecCall ["BIS_fnc_taskSetState",[0,-2] select isDedicated];
    [_host] joinSilent objNull;
    sleep 10;
    [] spawn DS_fnc_SendReward;
     if(DS_SIDEMISSIONS_ACTIVE_COUNT == 0) then {
    DS_SIDEMISSIONS_ACTIVE_COUNT = 0; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";
  } else {
    DS_SIDEMISSIONS_ACTIVE_COUNT = DS_SIDEMISSIONS_ACTIVE_COUNT - 1; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";
  };
    private ["_wp2"];
    
    _wp2 = (group _host) addWaypoint [getMarkerPos "reward_crew_despawn",0];
    _wp2 setWaypointType "MOVE";
    _wp2 setWaypointSpeed "LIMITED";
    _wp2 setWaypointBehaviour "CARELESS";
    _wp2 setWayPointCompletionRadius 50;
    _wp2 setWayPointStatements ["true", "{deleteVehicle _x} forEach thislist;"];
  };
};

