/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/12/2016
* Description     : Handles spawning a downed heli sideMission and task creation for the server
* **LOCALIZED**
*/
private ["_location", "_cityPOS", "_cityRadA", "_cityRadB", "_cache", "_group"];
private ["_taskName","_title","_brief","_taskMarker"];

switch (WorldName) do {
  case "Stratis": {_location = [[DS_HILL]]           call DS_fnc_findLocation;};
  case "Altis"  : {_location = [[DS_HILL]] call DS_fnc_findLocation;};
  case "Tanoa"  : {_location = [[DS_HILL]]                      call DS_fnc_findLocation;};
  default         {_location = [[DS_HILL]]          call DS_fnc_findLocation;};
};

//--- Get city position and radius
_cityPOS = _location select 1;
_cityRadA = (_location select 2) + 600;
_cityRadB = (_location select 3) + 600;

if (_cityRadB > _cityRadA) then { _cityRadA = _cityRadB; };

//--- Create the task & associated markers.
_taskName = format["TASK%1",random 1000];
_title = format [localize "str_DS_task_DownedHeliTitle", _location select 0];
_brief = format [localize "str_DS_task_DownedHeliBrief", _location select 0];

[west,[_taskName],[_brief,_title,""],_cityPos,"CREATED",1,true,"Destroy"] call bis_fnc_taskCreate;
DS_SIDEMISSIONS_ACTIVE_COUNT = DS_SIDEMISSIONS_ACTIVE_COUNT + 1; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";

_taskMarker = [_cityPos,_taskName,_cityRadA] call DS_fnc_AddMarker;
[[_taskMarker,_cityPOS,_cityRadA,"CIV",1000],"EAST"] call DS_fnc_aliveaddobjtoside;

sleep 1;
private ["_vehclass","_spawnLoc","_veh"];
_vehclass = DS_SM_HELICRASHVEH call BIS_fnc_selectrandom;
_spawnLoc = [_cityPOS, 0, 600, 2, 0, 100, 0] call BIS_fnc_findSafePos; //Location of first IED/Mine

//--- Create the Heli at the selected indoor position
_veh = createVehicle [_vehclass, _spawnLoc, [], 0, "None"];

if (DS_DEBUG) then {
	[(getpos _veh),"debug",1,"hd_dot","ColorRed",true,"Downed Heli"] call DS_fnc_AddMarker;
};

_veh setVariable ["marker",_taskMarker,false];
_veh setVariable ["tname",_taskName,false];
_veh lock 3; //Locked for player
_veh setDamage 0.9;
_veh setFuel 0;

_veh addEventHandler ["handledamage", {
	if ((_this select 4) in ["SatchelCharge_Remote_Ammo", "DemoCharge_Remote_Ammo", "SatchelCharge_Remote_Ammo_Scripted", "DemoCharge_Remote_Ammo_Scripted"]) then {
		(_this select 0) setdamage 1;
		private ["_passMarker"]; _passMarker = (_this select 0) getVariable "marker";
		private ["_tname"]; _tname = (_this select 0) getVariable "tname";
		//--- event handler for killing the vehicle
		[_this select 0,_passMarker,_tname] spawn DS_fnc_sm_end_helicrash;
		(_this select 0) removeAllEventHandlers "handledamage";
	}; }];

_veh addEventHandler ["killed", {
		private ["_passMarker"]; _passMarker = (_this select 0) getVariable "marker";
		private ["_tname"]; _tname = (_this select 0) getVariable "tname";
		[_this select 0,_passMarker,_tname] spawn DS_fnc_sm_end_helicrash;
	(	_this select 0) removeAllEventHandlers "killed"; 
	}];
//--- End of eventhandlers

//--- Disable simulation of the vehicle
_veh enableSimulationGlobal false;