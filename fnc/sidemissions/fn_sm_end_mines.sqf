/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/14/2016
* Description     : Mine Finish Script! 
*/
params ["_taskName","_markerName"];
  //------------------------------ ** END OF TASK CLEANUP--------------------------------------------------------------
   if(DS_SIDEMISSIONS_ACTIVE_COUNT == 0) then {
    DS_SIDEMISSIONS_ACTIVE_COUNT = 0; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";
  } else {
    DS_SIDEMISSIONS_ACTIVE_COUNT = DS_SIDEMISSIONS_ACTIVE_COUNT - 1; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";
  };
  [_markerName,"EAST"] remoteExecCall ["DS_fnc_aliveremoveobjectivefromsides",2];
  (_markerName) remoteExecCall ["deleteMarker",[0,2] select isDedicated];
  [_taskName, "Succeeded"] remoteExecCall ["BIS_fnc_taskSetState",[0,-2] select isDedicated];
  [] spawn DS_fnc_SendReward;
  //------------------------------ ** END OF TASK CLEANUP--------------------------------------------------------------
