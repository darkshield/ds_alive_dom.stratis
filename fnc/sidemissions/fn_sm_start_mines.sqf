/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/14/2016
* Description     : Handles spawning a minefield that must be disarmed! (or otherwise blown up)
* **LOCALIZED**
*/
private ["_mines","_cityLoc","_cityPOS","_cityRadA","_cityRadB","_mineMarks","_TaskID"];

switch (WorldName) do {
  case "Stratis": {_cityLoc = [[DS_LgCITY,DS_VILLAGE]]                      call DS_fnc_findLocation;};
  case "Altis"  : {_cityLoc = [[DS_LgCITY,DS_CITY,DS_VILLAGE,DS_SmCITY]] call DS_fnc_findLocation;};
  case "Tanoa"  : {_cityLoc = [[DS_LGCITY,DS_CITY]]                      call DS_fnc_findLocation;};
  default         {_cityLoc = [[DS_LGCITY,DS_CITY]]                      call DS_fnc_findLocation;};
};

_cityPOS  = _cityLoc select 1;
_cityRadA = (_cityLoc select 2) + 500;
_cityRadB = _cityLoc select 3;

private ["_taskName","_mrkName","_title","_brief"];
_taskName = format["TASK%1",random 1000];
_title = format [localize "str_DS_task_mineTitle",  _cityLoc select 0];
_brief = format [localize "str_DS_task_mineBrief",  _cityLoc select 0];
//[west,[_taskName],[_brief,_title,""],_cityPos,"CREATED",1,true,"Search"] call bis_fnc_taskCreate;
//_mrkName = [_cityPOS,_taskName,_cityRadA] call DS_fnc_AddMarker;


//[[_mrkName,_cityPOS,_cityRadA,"CIV",1000],"EAST"] call DS_fnc_aliveaddobjtoside;

private ["_numClusters","_blacklist","_roadlist","_spawnPos","_numInClusters","_newNodePos","_curRoadNode","_side","_mine","_mineCheck","_tot"];
_numClusters = (floor random 3) + 1; 
_numInClusters = (floor random 5) + 5;
_tot = _numClusters * _numInClusters;
_blacklist = [];

if (DS_DEBUG_SIDEMISSIONS) then {_cityPos = getMarkerPos "BLUFOR_1"};
_roadlist = _cityPos nearRoads 1000;  // amke sure to find at least ONE road node to start with
_curRoadNode = _roadlist call BIS_fnc_selectRandom;

while {[DS_BLUFOR_MARKER,position _curRoadNode] call BIS_fnc_InTrigger} do { _curRoadNode = _roadlist call BIS_fnc_selectRandom; };
        
//This node is where we will create the marker - lets just give them a 'gimmie' at the center.

[west,[_taskName],[_brief,_title,""],position _curRoadNode,"CREATED",1,true,"Search"] call bis_fnc_taskCreate;
_mrkName = [position _curRoadNode,_taskName,_cityRadA] call DS_fnc_AddMarker;
[[_mrkName,position _curRoadNode,_cityRadA,"CIV",1000],"EAST"] call DS_fnc_aliveaddobjtoside;
DS_SIDEMISSIONS_ACTIVE_COUNT = DS_SIDEMISSIONS_ACTIVE_COUNT + 1; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";

for "_i" from 1 to _numClusters do {
  _roadlist = [];
  for "_num" from 25 to 250 step 25 do {
    _roadlist = _curRoadNode nearRoads _num;
    _roadlist = _roadlist - _blacklist; //Remove our blacklisted locations!   
    if (count _roadlist > _numInClusters + 10 ) exitwith {true};   
  };
    // we have enough positions now!
  for "_in" from 1 to _numInClusters do {
    _curRoadNode = objNull;
    _curRoadNode = _roadlist call BIS_fnc_selectRandom;
    if (_curRoadNode == objNull) exitWith {
      if (DS_DEBUG) then {hint "DARKSHIELD_DOMINATION: fn_sm_start_mines.sqf - PROBLEM - NO current road node!";};
    };
      _spawnPos = [position _curRoadNode, 0, 10, 2, 0, 100, 0] call BIS_fnc_findSafePos; //Location of first IED/Mine
      _blacklist pushBack _curRoadNode;
      _mine = createMine [(DS_VANILLA_ROADMINE_OBJS call BIS_fnc_selectRandom), _spawnPos, [], 0];
      if (DS_DEBUG) then {[getPos _mine,"Mine",1,"hd_dot","ColorBlack",true,"Mine"] call DS_fnc_AddMarker;};
  };
};

sleep 10;

_mineCheckHB = [_mrkName,_tot,_taskName,_mrkName,_cityRadA] spawn {
  params ["_mrkName","_tot","_taskName","_mrkName","_radius"];
  private ["_c"]; _c = 0;
  private ["_cnt"]; _cnt = 0;
  private ["_mines"]; _mines = [];

  while {true} do {
    sleep 5;
    _cnt = 0;
    //DS_VANILLA_ROADMINE_PLACED
    {
      if (DS_DEBUG_SIDEMISSIONS) then {
      _mines = (getMarkerPos "BLUFOR_1") nearObjects [_x,_radius];
    } else {
      _mines = (getMarkerPos _mrkName) nearObjects [_x,_radius];
    };
      
      _cnt = _cnt + count _mines;
    } forEach DS_VANILLA_ROADMINE_PLACED;


    if (DS_DEBUG) then {hint format["%1/%2",_cnt,_tot];};
    if (_cnt == 0 && DS_DEBUG) then {hint format["No Mines found? Rad: %2 Weird %1",_taskName,_radius];};
    // _cnt now equals the mines remaining
    if ((_cnt / _tot) < 0.30 ) exitWith {
      [_taskName,_mrkName] spawn DS_fnc_sm_end_mines;
    };
    
  };
};