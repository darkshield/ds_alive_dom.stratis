/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : BBrown
* Last Editor     : BBrown
* Last Edited     : 09/15/2016
* Description     : Handles cleanup of convoy side mission
* USAGE           : [_taskName,"SUCCEEDED",_markerStart,_markerEnd] spawn DS_fnc_sm_end_convoy;
* **LOCALIZED**
*/

params [
        "_taskName",
       ["_completionState","UNKNOWN",[""]],
       "_markerStart",
       "_markerEnd"
       ];
//----------------------------------------------- ** END OF TASK CLEANUP--------------------------------------------------------------
if(DS_SIDEMISSIONS_ACTIVE_COUNT == 0) then {
  DS_SIDEMISSIONS_ACTIVE_COUNT = 0; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";
} else {
  DS_SIDEMISSIONS_ACTIVE_COUNT = DS_SIDEMISSIONS_ACTIVE_COUNT - 1; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";
};
switch (toUpper (_completionState)) do {
  case "SUCCEEDED": {
    [_taskName,"SUCCEEDED",true] spawn BIS_fnc_taskSetState;
    deleteMarker _markerStart;
    deleteMarker _markerEnd;
    [] spawn DS_fnc_sendReward;
  };
  case "FAILED": {
    [_taskName,"FAILED",true] spawn BIS_fnc_taskSetState;
    deleteMarker _markerStart;
    deleteMarker _markerEnd;
  };
  default {
    hint "DEBUG: Contact mission developers with error: 'Convoy End completion state'";
    ["fn_sm_end_convoy.sqf | UNKNOWN mission completion state, check fn_sm_start_convoy and ensure a completion state is being passed"] call ALiVE_fnc_Dump;
  };
};
//----------------------------------------------- ** END OF TASK CLEANUP--------------------------------------------------------------