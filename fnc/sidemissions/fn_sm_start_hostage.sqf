/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/18/2016
* Description     : Handles spawning of hostages in a city as a side Mission. Also some crazy outcomes.
* **LOCALIZED**
*/
private ["_areaName","_host","_unit","_loc","_house","_buildings","_spawnPOS","_m","_cityPos","_cityRadA","_cityRadB","_blacklistPOS"];
_blacklistPOS = [];
//--- Find a suitable location for the mission
switch (WorldName) do {
  case "Stratis": {_loc = [[DS_LGCITY,DS_VILLAGE]]        call DS_fnc_findLocation;};
  case "Altis"  : {_loc = [[DS_LGCITY]]                   call DS_fnc_findLocation;};
  case "Tanoa"  : {_loc = [[DS_LGCITY,DS_CITY]]           call DS_fnc_findLocation;};
  default         {_loc = [[DS_LGCITY,DS_CITY]]           call DS_fnc_findLocation;};
};
_cityPos = _loc select 1;
_cityRadA = (_loc select 2) - 100;
_cityRadB = _loc select 3;

if (_cityRadB > _cityRadA) then { _cityRadA = _cityRadB; };

//-- Check location type to determine how to present the information
if (_loc select 4 == "Hill") then {
  _areaName = localize "str_DS_hill_Wording"; //TODO: NEEDS LOCALIZED
  _house = _loc select 1;
  _spawnPOS = _house;
} else {
  _areaName = format ["%1 %2",localize "str_DS_Near",_loc select 0];
  _buildings = [_loc select 1, _cityRadA] call DS_fnc_getEnterableBldgs;
  if (count _buildings == 0) exitWith { [] call DS_fnc_sm_start_KillHVT; };
  _house = _buildings call bis_fnc_selectRandom;
  _spawnPOS = [_house] call DS_fnc_findRandBldgPos;
  _blacklistPOS pushBack _spawnPOS;
};

private ["_wgrp","_egrp"];
_egrp = createGroup east;
_wgrp = createGroup west;
if (DS_DEBUG_SIDEMISSIONS) then {_spawnPOS = getMarkerPos "BLUFOR_1"};
_host = _wgrp createUnit ["B_officer_F", _spawnPOS,[], 0, "NONE"]; //--- Spawn the hostage within the house.
_host setcaptive true;
_host disableAI "AUTOTARGET";
_host disableAI "MOVE";
sleep 4;
_host switchMove "boundCaptive_loop";
_host playMove "boundCaptive_loop"; // UNDO : boundCaptive_unaErc


_spawnPOS = [_house] call DS_fnc_findRandBldgPos;
if (!([_spawnPOS] in _blacklistPOS)) then {
  if (DS_DEBUG_SIDEMISSIONS) then {_spawnPOS = getMarkerPos "BLUFOR_1"};
  _unit = _egrp createUnit ["O_officer_F", _spawnPOS,[], 0, "NONE"]; //--- Spawn the enemy's within the house.
};

sleep 5;
if !(alive _unit && alive _host) exitWith {
  deleteVehicle _unit;
  deleteVehicle _host;
  [] spawn DS_fnc_sm_start_hostage; 
};


removeAllWeapons       _host;
removeAllItems         _host;
removeAllAssignedItems _host;
removeVest             _host;
removeBackpack         _host;
removeHeadgear         _host;

 	
//--- Create some lackeys to protect him.
_spawnPOS = [_house] call DS_fnc_findRandBldgPos;
if (!([_spawnPOS] in _blacklistPOS)) then {
  if (DS_DEBUG_SIDEMISSIONS) then {_spawnPOS = getMarkerPos "BLUFOR_1"};
  _egrp createUnit ["O_T_Recon_Medic_F",_spawnPOS,[],0,"NONE"];
  _blacklistPOS pushBack _spawnPOS;
};
//--- Create some lackeys to protect him.
 _spawnPOS = [_house] call DS_fnc_findRandBldgPos;
if (!([_spawnPOS] in _blacklistPOS)) then {
  if (DS_DEBUG_SIDEMISSIONS) then {_spawnPOS = getMarkerPos "BLUFOR_1"};
  _egrp createUnit ["O_T_Soldier_GL_FF",_spawnPOS,[],0,"NONE"];
  _blacklistPOS pushBack _spawnPOS;
};
//--- Create some lackeys to protect him.
_spawnPOS = [_house] call DS_fnc_findRandBldgPos;
if (!([_spawnPOS] in _blacklistPOS)) then {
  if (DS_DEBUG_SIDEMISSIONS) then {_spawnPOS = getMarkerPos "BLUFOR_1"};
  _egrp createUnit ["O_T_Soldier_GL_FF",_spawnPOS,[],0,"NONE"];
  _blacklistPOS pushBack _spawnPOS;
};

//--- Create the task & associated markers.
private ["_taskName","_mrkName"];
_taskName = format["TASK%1",random 1000];
_title = format [localize "str_DS_task_hostageTitle", _areaName];
_brief = format [localize "str_DS_task_hostageBrief", _areaName];
[west,[_taskName],[_brief,_title,""],_cityPos,"CREATED",1,true,"Attack"] call bis_fnc_taskCreate;
DS_SIDEMISSIONS_ACTIVE_COUNT = DS_SIDEMISSIONS_ACTIVE_COUNT + 1; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";
_mrkName = [_cityPos,_taskName,_cityRadA] call DS_fnc_AddMarker;
[[_mrkName,_cityPOS,_cityRadA,"CIV",1000],"EAST"] call DS_fnc_aliveaddobjtoside;


if (DS_DEBUG) then { [(getpos _hvt),"debug",1,"hd_dot","ColorRed",true,"HVT"] call DS_fnc_AddMarker; };

//--- create interaction on this guy to call the sm_end script.

  _actID = [_host,                                                   // Object the action is attached to
    localize "str_DS_ReleaseTarget",                                     // Title of the action 
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_unbind_ca.paa",       // Idle icon shown on screen
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_unbind_ca.paa",       // Progress icon shown on screen
    "(_this distance _target < 2) && (alive _target)",                   // Condition for the action to be shown
    "(_this distance _target < 2) && (alive _target)",                   // Condition for the action to progress
    {},                                                                  // Code executed when action starts
    {},                                                                  // Code executed on every progress tick
    {
      _this spawn DS_fnc_sm_end_hostage;
      [(_this select 0),(_this select 2)] remoteExec ["bis_fnc_holdActionRemove",[0,-2] select isDedicated,_this select 0];
    },                                                                   // Code executed on completion
    {},                                                                  // Code executed on interrupted
    [_host,_taskName,_mrkName,"Alive"],                                   // Arguments passed to the scripts as _this select 3
    5,                                                                   // Action duration [s]
    0,                                                                   // Priority
    true,                                                                // Remove on completion
    false                                                                // Show in unconscious state 
    ] remoteExecCall ["BIS_fnc_holdActionAdd",[0,-2] select isDedicated,_host];  // example for MP compatible implementation
  

    [_host,                                                   // Object the action is attached to
    localize "str_DS_IDTarget",                                     // Title of the action 
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",       // Idle icon shown on screen
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",       // Progress icon shown on screen
    "(_this distance _target < 2) && !(alive _target)",    //He dead     // Condition for the action to be shown
    "(_this distance _target < 2) && !(alive _target)",    //He dead     // Condition for the action to progress
    {},                                                                  // Code executed when action starts
    {},                                                                  // Code executed on every progress tick
    {
      _this spawn DS_fnc_sm_end_hostage;
      [(_this select 0),(_this select 2)] remoteExec ["bis_fnc_holdActionRemove",[0,-2] select isDedicated,_this select 0];
    },                                                                   // Code executed on completion
    {},                                                                  // Code executed on interrupted
    [_host,_taskName,_mrkName,"Dead"],                                  // Arguments passed to the scripts as _this select 3
    3,                                                                   // Action duration [s]
    0,                                                                   // Priority
    true,                                                                // Remove on completion
    false                                                                // Show in unconscious state 
    ] remoteExecCall ["BIS_fnc_holdActionAdd",[0,-2] select isDedicated,_host];  // example for MP compatible implementation

    [_host,                                                   // Object the action is attached to
    localize "str_DS_ReturnedToBase",                                     // Title of the action 
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",       // Idle icon shown on screen
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",       // Progress icon shown on screen
    "(_this distance _target < 2) && (alive _target) && (['BLUFOR_1',_this] call BIS_fnc_InTrigger)",        // Condition for the action to be shown
    "(_this distance _target < 2) && (alive _target) && (['BLUFOR_1',_this] call BIS_fnc_InTrigger)",        // Condition for the action to progress
    {},                                                                  // Code executed when action starts
    {},                                                                  // Code executed on every progress tick
    {
      _this spawn DS_fnc_sm_end_hostage;
      [(_this select 0),(_this select 2)] remoteExec ["bis_fnc_holdActionRemove",[0,-2] select isDedicated,_this select 0];
    },                                                                   // Code executed on completion
    {},                                                                  // Code executed on interrupted
    [_host,_taskName,_mrkName,"Returned"],                               // Arguments passed to the scripts as _this select 3
    3,                                                                   // Action duration [s]
    0,                                                                   // Priority
    true,                                                                // Remove on completion
    false                                                                // Show in unconscious state 
    ] remoteExecCall ["BIS_fnc_holdActionAdd",[0,-2] select isDedicated,_host];  // example for MP compatible implementation