/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/12/2016
* Description     : Handles spawning a weapon cache sideMission and task creation for the server
* **LOCALIZED**
*/
private ["_location", "_buildings", "_bldg", "_bldgPos", "_cityPOS", "_cityRadA", "_cityRadB", "_cache", "_group"];
private ["_taskName","_title","_brief","_taskMarker"];

switch (WorldName) do {
  case "Stratis": {_location = [[DS_LGCITY,DS_CITY,DS_VILLAGE]]           call DS_fnc_findLocation;};
  case "Altis"  : {_location = [[DS_LgCITY,DS_CITY,DS_VILLAGE,DS_SmCITY]] call DS_fnc_findLocation;};
  case "Tanoa"  : {_location = [[DS_LGCITY,DS_CITY]]                      call DS_fnc_findLocation;};
  default         {_location = [[DS_LGCITY,DS_CITY,DS_VILLIAGE]]          call DS_fnc_findLocation;};
};

//--- Get city position and radius
_cityPOS = _location select 1;
_cityRadA = (_location select 2) + 400;
_cityRadB = (_location select 3) + 400;

if (_cityRadB > _cityRadA) then { _cityRadA = _cityRadB; };

//--- Find enterable houses
_buildings = [_cityPOS, _cityRadA] call DS_fnc_getEnterableBldgs;

//--- Check we can complete the function - if not loop back around.
if (isNil "_buildings")    exitWith { [] spawn DS_sm_start_wepcache; };
if (count _buildings == 0) exitWith { [] spawn DS_sm_start_wepcache; };

//--- Select a random enterable house
_bldg = _buildings call bis_fnc_selectRandom;

//--- Finding random indoor position for spawning the cache
_bldgPos = [_bldg] call DS_fnc_findRandBldgPos;
if (DS_DEBUG_SIDEMISSIONS) then {_bldPos = getMarkerPos "BLUFOR_1"};
private ["_overWater"];
_overWater = !(_bldgPos isFlatEmpty  [-1, -1, -1, -1, 2, false] isEqualTo []);

//--- Check to make sure we don't spawn a cache on a pier or in water.
if (_overWater) exitWith { [] spawn DS_fnc_sm_start_wepcache; };

//--- Create the task & associated markers.
_taskName = format["TASK%1",random 1000];
_title = format [localize "str_DS_task_CacheTitle", _location select 0];
_brief = format [localize "str_DS_task_CacheBrief", _location select 0];

[west,[_taskName],[_brief,_title,""],_cityPos,"CREATED",1,true,"Destroy"] call bis_fnc_taskCreate;
DS_SIDEMISSIONS_ACTIVE_COUNT = DS_SIDEMISSIONS_ACTIVE_COUNT + 1; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";

_taskMarker = [_cityPos,_taskName,_cityRadA] call DS_fnc_AddMarker;
[[_taskMarker,_cityPOS,_cityRadA,"CIV",1000],"EAST"] call DS_fnc_aliveaddobjtoside;




sleep 1;

//--- Create the cache at the selected indoor position
_cache = createVehicle ["Box_FIA_Wps_F", _bldgPos, [], 0, "None"];
if (DS_DEBUG) then {
	[(getpos _cache),"debug",1,"hd_dot","ColorRed",true,"Cache"] call DS_fnc_AddMarker;
};

/*
* 1: _pos       - [REQUIRED]
* 2: _taskName  - [OPTIONAL] STRING - Prefix of the marker name to be found later.
* 3: _radius    - [OPTIONAL] NUMBER - Radius for Marker's Circle - Default: 100
* 4: _type      - [OPTIONAL] STRING - "ELLIPSE" or "DOT" - Default: "ELLIPSE"
* 5: _color     - [OPTIONAL] STRING - "GREEN" - Default: "RED"
* 6: _debug     - [OPTIONAL] BOOL   - Is this is a debug marker? Default: false
* 7: _debugtext - [OPTIONAL] STRING - Text to show next to icon - Default: "Debug"
*/


//-- Empty the cache... doesn't this defeat the purpose of destroying it? sarcasm = true
clearMagazineCargoGlobal _cache;
clearWeaponCargoGlobal _cache;

//--- Add eventhandlers to the cache
//--- Handle damage only for Satchels and Demo charges

_cache setVariable ["marker",_taskMarker,false];
_cache setVariable ["tname",_taskName,false];

_cache addEventHandler ["handledamage", {
	if ((_this select 4) in ["SatchelCharge_Remote_Ammo", "DemoCharge_Remote_Ammo", "SatchelCharge_Remote_Ammo_Scripted", "DemoCharge_Remote_Ammo_Scripted"]) then {
		(_this select 0) setdamage 1;
		private ["_passMarker"]; _passMarker = (_this select 0) getVariable "marker";
		private ["_tname"]; _tname = (_this select 0) getVariable "tname";
		//--- event handler for killing the cache
		[_this select 0,_passMarker,_tname,_this select 3] spawn DS_fnc_sm_end_wepcache;
	} else {
		(_this select 0) setdamage 0;
}}];
//--- End of eventhandlers

//--- Disable simulation of the cache
_cache enableSimulationGlobal false;