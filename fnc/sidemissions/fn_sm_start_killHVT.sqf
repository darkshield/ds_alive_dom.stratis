/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/13/2016
* Description     : Handles spawning a High Value Target in a city as a side Mission. He's dangerous :)
* **LOCALIZED**
*/
private ["_areaName","_hvt","_loc","_house","_buildings","_spawnPOS","_m","_cityPos","_cityRadA","_cityRadB","_blacklistPOS"];
_blacklistPOS = [];
//--- Find a suitable location for the mission
switch (WorldName) do {
  case "Stratis": {_loc = [[DS_LGCITY,DS_VILLAGE]]        call DS_fnc_findLocation;};
  case "Altis"  : {_loc = [[DS_LGCITY]]                   call DS_fnc_findLocation;};
  case "Tanoa"  : {_loc = [[DS_LGCITY,DS_CITY]]           call DS_fnc_findLocation;};
  default         {_loc = [[DS_LGCITY,DS_CITY]]           call DS_fnc_findLocation;};
};
_cityPos = _loc select 1;
_cityRadA = (_loc select 2) + 400;
_cityRadB = _loc select 3;

if (_cityRadB > _cityRadA) then { _cityRadA = _cityRadB; };

//-- Check location type to determine how to present the information
if (_loc select 4 == "Hill") then {
  _areaName = localize "str_DS_hill_Wording"; //TODO: NEEDS LOCALIZED
  _house = _loc select 1;
  _spawnPOS = _house;
} else {
  _areaName = format ["%1 %2",localize "str_DS_Near",_loc select 0];
  _buildings = [_loc select 1, _cityRadA] call DS_fnc_getEnterableBldgs;
  if (count _buildings == 0) exitWith { [] call DS_fnc_sm_start_KillHVT; };
  _house = _buildings call bis_fnc_selectRandom;
  _spawnPOS = [_house] call DS_fnc_findRandBldgPos;
  _blacklistPOS pushBack _spawnPOS;
};

if (DS_DEBUG_SIDEMISSIONS) then {_spawnPOS = getMarkerPos "BLUFOR_1"};

private ["_grp"];
_grp = createGroup east;	//TODO: Change to OPFOR Group
_hvt = _grp createUnit ["O_officer_F", _spawnPOS,[], 0, "NONE"]; //--- Spawn the HVT

 	
//--- Check if he's still alive... cuz he may have fell... silly goose.
sleep 5;
if !(alive _hvt) exitWith {
  deleteVehicle _hvt;
  [] spawn DS_fnc_sm_start_KillHVT; 
};

//--- Create some lackeys to protect him.
_spawnPOS = [_house] call DS_fnc_findRandBldgPos;
if (!([_spawnPOS] in _blacklistPOS)) then {
  _grp createUnit ["O_T_Recon_Medic_F",_spawnPOS,[],0,"NONE"];
  _blacklistPOS pushBack _spawnPOS;
};
//--- Create some lackeys to protect him.
 _spawnPOS = [_house] call DS_fnc_findRandBldgPos;
if (!([_spawnPOS] in _blacklistPOS)) then {
  _grp createUnit ["O_T_Soldier_GL_FF",_spawnPOS,[],0,"NONE"];
  _blacklistPOS pushBack _spawnPOS;
};
//--- Create some lackeys to protect him.
_spawnPOS = [_house] call DS_fnc_findRandBldgPos;
if (!([_spawnPOS] in _blacklistPOS)) then {
  _grp createUnit ["O_T_Soldier_GL_FF",_spawnPOS,[],0,"NONE"];
  _blacklistPOS pushBack _spawnPOS;
};

//--- Make our HVT special.
removeAllWeapons       _hvt;
removeAllItems         _hvt;
removeAllAssignedItems _hvt;
removeVest             _hvt;
removeBackpack         _hvt;
removeHeadgear         _hvt;
removeGoggles          _hvt;
_hvt addVest              "V_BandollierB_oli";
_hvt addHeadgear          "H_Beret_blk";
_hvt addGoggles           "G_Bandanna_aviator";
_hvt addWeapon            "srifle_DMR_03_khaki_F";
_hvt addPrimaryWeaponItem "optic_ACO_grn";
_hvt addWeapon            "Binocular";
_hvt linkItem             "ItemMap";
_hvt linkItem             "ItemCompass";
//_hvt setBehaviour         "COMBAT";
//_hvt setCombatMode        "RED";
for "_i" from 1 to 5 do {_hvt addItemToVest "20Rnd_762x51_Mag";};

_grp setBehaviour         "COMBAT";

//--- Create the task & associated markers.
private ["_taskName","_mrkName"];
_taskName = format["TASK%1",random 1000];
_title = format [localize "str_DS_task_killHVTTitle", _areaName];
_brief = format [localize "str_DS_task_killHVTBrief", _areaName];
[west,[_taskName],[_brief,_title,""],_cityPos,"CREATED",1,true,"Attack"] call bis_fnc_taskCreate;
DS_SIDEMISSIONS_ACTIVE_COUNT = DS_SIDEMISSIONS_ACTIVE_COUNT + 1; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";
_mrkName = [_cityPos,_taskName,_cityRadA] call DS_fnc_AddMarker;
[[_mrkName,_cityPOS,_cityRadA,"CIV",1000],"EAST"] call DS_fnc_aliveaddobjtoside;


if (DS_DEBUG) then { [(getpos _hvt),"debug",1,"hd_dot","ColorRed",true,"HVT"] call DS_fnc_AddMarker; };

//--- create interaction on this guy to call the sm_end script.
    [_hvt,                                                               // Object the action is attached to
    localize "str_DS_IDTarget",                                          // Title of the action 
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",       // Idle icon shown on screen
    "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_search_ca.paa",       // Progress icon shown on screen
    "_this distance _target < 2",                                        // Condition for the action to be shown
    "_caller distance _target < 2",                                      // Condition for the action to progress
    {},                                                                  // Code executed when action starts
    {},                                                                  // Code executed on every progress tick
    {_this spawn DS_fnc_sm_end_killHVT},                                 // Code executed on completion
    {},                                                                  // Code executed on interrupted
    [_hvt,_taskName,_mrkName],                                           // Arguments passed to the scripts as _this select 3
    5,                                                                   // Action duration [s]
    0,                                                                   // Priority
    true,                                                                // Remove on completion
    false                                                                // Show in unconscious state 
    ] remoteExecCall ["BIS_fnc_holdActionAdd",[0,-2] select isDedicated,_hvt];  // example for MP compatible implementation