/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/12/2016
* Description     : Handles selecting a random sidemission to spawn!
*/ 
private ["_x"];
_x = floor random 4;
switch (_x) do {
  case 0: { [] spawn DS_fnc_sm_start_wepcache; };
  case 1: { [] spawn DS_fnc_sm_start_killHVT;  };
  case 2: { [] spawn DS_fnc_sm_start_mines;    };
  case 3: { [] spawn DS_fnc_sm_start_helicrash;};
  case 4: { [] spawn DS_fnc_sm_start_hostage;};
};

if (DS_DEBUG_SIDEMISSIONS) then {
  [] spawn DS_fnc_sm_start_wepcache; 
  [] spawn DS_fnc_sm_start_killHVT;  
  [] spawn DS_fnc_sm_start_mines;    
  [] spawn DS_fnc_sm_start_helicrash;
  [] spawn DS_fnc_sm_start_hostage;
};