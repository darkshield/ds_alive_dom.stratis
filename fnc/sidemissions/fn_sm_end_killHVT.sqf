/*
* **LOCALIZED**
*/
//[_unit] spawn DS_fnc_sm_end_KillHVT;
private["_hvt","_taskName","_mrkName","_bombTimer","_bombSetOff"];
_hvt      = (_this select 3) select 0;
_taskName = (_this select 3) select 1;
_mrkName  = (_this select 3) select 2;

_bombSetOff = 0; 
// 0 = No Bomb 
// 1 = Bomb will explode with squad notification            "SUICIDE VEST - BOMB! RUN!"
// 2 = Bomb will IMMEDIATELYexplode with squad notification "BOM...<EXPLOSION>:

private ["_x"]; _x = floor random 100; 
if (_x <  60)  then {_bombSetOff = 1};
if (_x <  5 )  then {_bombSetOff = 2};
if (_x >= 60)  then {_bombSetOff = 0};

if (_bombSetOff == 2) then {_bombTimer = 0.2; };
if (_bombSetOff == 1) then {_bombTimer = (floor random 5) + 5}; // Max 25 seconds.
  
if (_bombSetOff != 0) then {
  if (_bombSetOff == 2) then {
    [profileName, localize "str_DS_task_killHVT_BombImmediate"] remoteExec ["BIS_fnc_showSubtitle",group player];
  } else {
    [profileName, localize "str_DS_task_killHVT_BombDelayed"] remoteExec ["BIS_fnc_showSubtitle",group player];
  };
  sleep _bombTimer;
  "M_Mo_82mm_AT_LG" createVehicle (getPos _hvt);
  sleep 1;
  "M_Mo_82mm_AT_LG" createVehicle (getPos _hvt);
  deletevehicle _hvt;
};
   if(DS_SIDEMISSIONS_ACTIVE_COUNT == 0) then {
    DS_SIDEMISSIONS_ACTIVE_COUNT = 0; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";
  } else {
    DS_SIDEMISSIONS_ACTIVE_COUNT = DS_SIDEMISSIONS_ACTIVE_COUNT - 1; publicVariable "DS_SIDEMISSIONS_ACTIVE_COUNT";
  };
  [_mrkName,"EAST"]  remoteExecCall ["DS_fnc_aliveremoveobjectivefromsides",2];
  _mrkName remoteExecCall ["deleteMarker",[0,2] select isDedicated];
  [_taskName, "Succeeded"] remoteExecCall ["BIS_fnc_taskSetState",[0,-2] select isDedicated];
  sleep 20;
  [] spawn DS_fnc_SendReward;