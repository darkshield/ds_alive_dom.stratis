/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/12/2016
* Description     : Returns a random position inside a building
* Args:
* 1: (REQUIRED) <OBJECT> - Building to process
*
* Return Value: <ARRAY> - Single Position in building
*
* Example: 
* [_building] call DS_fnc_findRandBldgPos;
*/
private ["_building","_count","_position","_z","_posATL"];

_z = 0;
_building = _this select 0;
_count = 1;

// Get Count of Building Positions
while {str(_building buildingPos _count) != "[0,0,0]"} do {
    _count = _count + 1;
};
// End getting a count of building Positions

if (_count == 0) then {
    _position = getPos _building;
    _posATL = getPosATL _building;
    _z = _posATL select 2;
} else {
    _position = random _count;
    _position = _building buildingPos _position;
    _z = _position select 2;   
};
if ((_position select 0) == 0) then {
    _position = getPos _building;
};

_position set [2,abs(_position select 2)];

_position