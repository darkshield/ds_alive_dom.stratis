/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/12/2016
* Description     : Handles grabbing a Location from the map based on it's cfgWorld
* Args:
* 1 (REQUIRED) <STRING> - String from common-defines such as DS_LGCITY
*
* Return Value: <ARRAY> - Location of City
*
* Example:
* [DS_LGCITY] call DS_fnc_findLocation;  
*/
private ["_locations","_cityTypes","_randomLoc","_x","_i","_cities"];
_cityTypes = _this select 0;
_i = 0;
_cities = [];
_locations = configFile >> "CfgWorlds" >> worldName >> "Names";

for "_x" from 0 to (count _locations - 1) do {
    private ["_cityName","_cityPos","_cityRadA","_cityRadB","_cityType"];

    _randomLoc = _locations select _x;
    _cityName  = getText  (_randomLoc >> "name");
    _cityPos   = getArray (_randomLoc >> "position");
    _cityRadA  = getNumber(_randomLoc >> "radiusA");
    _cityRadB  = getNumber(_randomLoc >> "radiusB");
    _cityType  = getText  (_randomLoc >> "type");

    if(_cityType in _cityTypes) then {
        _cities set [_i,[_cityName,_cityPos,_cityRadA,_cityRadB,_cityType]];
        _i = _i + 1;
    };
};

_randTown = _cities call bis_fnc_selectRandom;
_rtPos = _randTown select 1;
_rtRadA = _randTown select 2;
_rtRadB = _randTown select 3;

//--- Cut the radius values in half
_rtRadA = _rtRadA / 2;
_rtRadB = _rtRadB / 2;

//-- retrieve the x and y positions
_rtPosX = _rtPos select 0;
_rtPosY = _rtPos select 1;

//--- calculate new x and y positions
_rtPosX = _rtPosX - _rtRadA;
_rtPosY = _rtPosY - _rtRadB;

//--- apply new coords
_randTown set [1,[_rtPosX,_rtPosY]];
_randTown