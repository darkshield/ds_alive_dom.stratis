/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/12/2016
* Description     : Returns buildings that have buildingPos setup (and thus means are enterable)
* Example:
* _buildings = [_cityPOS, _cityRadA] call DS_fnc_getEnterableBldgs;
*/
private ["_cityPos","_cityRadA","_buildings","_enterablebuildings"];
_cityPos    = _this select 0;
_cityRadA   = _this select 1;
_buildings = [_cityPos,_cityRadA] call DS_fnc_findBuildings;
_enterablebuildings = [];
if (count _buildings >= 1) then { //--- If buildings were actually found, check if they're enterable and then add them to _enterablebuildings
  {
    _pos = _x buildingPos 1;
    if (str(_pos) != "[0,0,0]") then {
      _enterablebuildings = _enterablebuildings + [_x];
    };
  } forEach _buildings;
};
_enterablebuildings //return array of enterable buildings