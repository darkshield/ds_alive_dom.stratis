/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/13/2016
* Description     : Provides alive profiles enroute to a (to be deleted) objective some re-routing.
* 
* RETURN: NOTHING
* USAGE : [_opcom,_objectiveID,_reroute] call DS_fnc_alivehaltprofilesenrouteto
*/
if (isServer || isDedicated) then {
  private["_wp","_opcom","_objective","_objectiveID","_objectives","_sections","_profile","_profileID","_retreatToNearestHeldObj","_results","_pos","_resultPos"];
  _opcom = _this select 0;
  _objectiveID = _this select 1;
  _retreatToNearestHeldObj = [_this,2,false,[false]] call BIS_fnc_Param; // TODO: REWRITE

  _objectives = [_opcom,"objectives",[]] call ALiVE_fnc_HashGet;
  {
    _objective = _x;
    if(_objectiveID == [_objective,"objectiveID",""] call ALiVE_fnc_HashGet) then {
      _sections = [_objective,"section",[]] call ALiVE_fnc_HashGet;
      {
        _profile = [ALIVE_profileHandler, "getProfile", _x] call ALIVE_fnc_profileHandler;
        _profileID = [_profile,"profileID",""] call ALiVE_fnc_HashGet;
        [_profile,"clearWaypoints"] call ALIVE_fnc_profileEntity;
//        diag_log format["Halting %1 that was headed to %2",_profileID,_objectiveID];

        if(_retreatToNearestHeldObj) then {
          _pos = [_profile,"position",[]] call ALiVE_fnc_HashGet;
//          diag_log format ["Finding an objective to reroute %1",_profileID];
          _results = [_pos, _objectives, [_objectiveID]] call DS_fnc_alivenearestObjToIgnoring;

          if(typename (_results select 0) != "STRING") then {
//            diag_log format ["Routing %1 to %2",_profileID, [(_results select 0),"objectiveID",""] call ALiVE_fnc_HashGet];
            _resultPos = [(_results select 1)] call DS_fnc_aliverandomPointNear;
            _wp = [_resultPos, 50, "MOVE","NORMAL",25] call ALIVE_fnc_createProfileWaypoint;
            [_profile,"addWaypoint",_wp] call ALIVE_fnc_profileEntity;
          } else {
//            diag_log format ["Could not find an objective for %1",_profileID];
          };
        };
      } forEach _sections;
    };
  } forEach _objectives;
};