/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/13/2016
* Description     : Removes TAOR objective from OPCOM of specified side
*
* RETURN: NOTHING
* USAGE : [_TAORmarker,true|false] call DS_fnc_aliveremoveobjectivefromsides
*/

if (isServer || isDedicated) then {
  private["_objectiveID","_factionSide","_opcom","_opcomSide","_baseObjName","_reroute"];
  _baseObjName = _this select 0;
  _factionSide = _this select 1;
  _reroute = true;
  //deleteMarker _baseObjName;
  {
    _opcom = _x;
    _opcomSide = [_opcom,"side",""] call ALiVE_fnc_HashGet;
    _objectiveID = format["ALiVE_OPCOM_%1_%2",_baseObjName,_opcomSide];
    //diag_log format ["Trying to remove %1 marker",_objectiveID];
    if( _opcomSide == _factionSide) then {
      // find active Logistics modules, pause them, wait until they are paused and then do the remove
      [_opcom,_objectiveID,_reroute] call DS_fnc_alivehaltProfilesEnrouteTo;
      [_opcom, "removeObjective", _objectiveID] call ALiVE_fnc_OPCOM;
//      diag_log format["Removed %1",_objectiveID];
    };
  } forEach OPCOM_INSTANCES;
};