/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/13/2016
* Description     : Adds a CIV OPCOM objective with given radius to every OPCOM instance in the server.
* RETURN: NOTHING
* USAGE : [_marker, _position, _radius, CIV | MIL, Priority] call DS_fnc_aliveaddobjtoside
*/

if (isServer || isDedicated) then {
  private["_objectiveParams","_factionSide","_faction","_opcom","_opcomSide","_baseObjName"];
  _objectiveParams = _this select 0;
  _factionSide = _this select 1;
  _baseObjName = _objectiveParams select 0;
  {
    _opcom = _x;
    _opcomSide = [_opcom,"side",""] call ALiVE_fnc_HashGet;
    _objectiveParams set[0, format["%1_%2",_baseObjName,_opcomSide]];
    if( _opcomSide == _factionSide) then {
      [_opcom, "addObjective", _objectiveParams] call ALiVE_fnc_OPCOM;
    };
  } forEach OPCOM_INSTANCES;
};