/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/18/2016
* Description     : Checks for toolkit before allowing nearby vehicle.
*/

if ("ToolKit" in (items player)) then {
  private ["_vehs","_veh","_curdmg","_vehDesc"]; 
  _vehs = nearestObjects [player, ["Car","Tank","Ship","Air"], 5];
  _veh = _vehs select 0;
  _vehDesc = (typeOf _veh) call ISSE_Cfg_Vehicle_GetName;
  _curdmg = damage _veh;
  if (DS_DEBUG) then {hint format["Repair: %1 on %2",_curdmg,_vehDesc];};
  _veh setDamage _curdmg - 0.05;
} else {
  //No Toolkit - let the user know?
};