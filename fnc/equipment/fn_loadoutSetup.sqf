/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : BBrown
* Last Editor     : BBrown
* Last Edited     : 09/10/2016
* Description     : Initializes the roles/loadouts and makes them available in mission
***/
[missionNamespace,"RiflemanBasic"]             call BIS_fnc_addRespawnInventory;
[missionNamespace,"MachinegunnerBasic"]        call BIS_fnc_addRespawnInventory;
[missionNamespace,"AsstMachinegunnerBasic"]    call BIS_fnc_addRespawnInventory;
[missionNamespace,"RiflemanATBasic"]           call BIS_fnc_addRespawnInventory;
[missionNamespace,"SquadLeaderBasic"]          call BIS_fnc_addRespawnInventory;
[missionNamespace,"TeamLeaderBasic"]           call BIS_fnc_addRespawnInventory;
[missionNamespace,"MedicBasic"]                call BIS_fnc_addRespawnInventory;
[missionNamespace,"ExplosiveSpecialistBasic"]  call BIS_fnc_addRespawnInventory;
[missionNamespace,"MarksmanBasic"]             call BIS_fnc_addRespawnInventory;
[missionNamespace,"SniperBasic"]               call BIS_fnc_addRespawnInventory;
[missionNamespace,"SpotterBasic"]              call BIS_fnc_addRespawnInventory;
[missionNamespace,"PilotBasic"]                call BIS_fnc_addRespawnInventory;
[missionNamespace,"PilotFixedBasic"]           call BIS_fnc_addRespawnInventory;
[missionNamespace,"TeamLeaderReconBasic"]      call BIS_fnc_addRespawnInventory;
[missionNamespace,"ReconScoutBasic"]           call BIS_fnc_addRespawnInventory;
[missionNamespace,"ReconMedicBasic"]           call BIS_fnc_addRespawnInventory;
[missionNamespace,"ReconSharpshooterBasic"]    call BIS_fnc_addRespawnInventory;
[missionNamespace,"ReconUAVOpBasic"]           call BIS_fnc_addRespawnInventory;