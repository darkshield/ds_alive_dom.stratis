/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/14/2016
* Description     : Checks for toolkit before allowing to disarm a mine.
*/

if ("ToolKit" in (items player)) then {
  if !((player nearObjects ['MineGeneric',3] select 0) isequalto objnull) exitWith {
    player action ["deactivate",player,(player nearObjects ['MineGeneric',3] select 0)];
  };
    if !((player nearObjects ['MineBase',3] select 0) isequalto objnull) exitWith {
    player action ["deactivate",player,(player nearObjects ['MineBase',3] select 0)];
  };
  if !((player nearObjects ['TimeBombCore',3] select 0) isequalto objnull) exitWith {
    player action ["deactivate",player,(player nearObjects ['TimeBombCore',3] select 0)];
  };
  //player action ["deactivate",player,nearestObject [player, "TimeBombCore"]];
} else {
  //No Toolkit - let the user know?
};