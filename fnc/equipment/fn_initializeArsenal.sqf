/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : BBrown
* Last Editor     : BBrown
* Last Edited     : 09/12/2016
* Description     : Initializes the Virtual Arsenal and adds all whitelisted items (found in equipment.hpp)

USAGE: In init line of Ammo box: 
    nul = [this] execVM "fnc\equipment\fn_initializeArsenal.sqf";
***/
#include "..\..\data\equipment.hpp";

//Initialize Arsenal Functionality
["AmmoBoxInit",[(_this select 0),false]] spawn BIS_fnc_arsenal;

//Add weapon cargo
[(_this select 0),DS_VA_WEAPONCARGO_BLU,true] call BIS_fnc_addVirtualWeaponCargo;
//Add magazine cargo
[(_this select 0),DS_VA_MAGAZINECARGO_BLU,true] call BIS_fnc_addVirtualMagazineCargo;
//Add item cargo
[(_this select 0),DS_VA_ITEMCARGO_BLU,true] call BIS_fnc_addVirtualItemCargo;
//Add backpack cargo
[(_this select 0),DS_VA_BACKPACKCARGO_BLU,true] call BIS_fnc_addVirtualBackpackCargo;