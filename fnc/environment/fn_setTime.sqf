/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/10/2016
* Description     : Handles mission time set when starting the mission.
*/

if(!DS_SRV) exitwith {setDate date};

private["_year","_month","_day","_hour","_min"];

_year	  = if(count _this > 2) then { _this select 0 }else{ date select 0 };
_month	= if(count _this > 2) then { _this select 1 }else{ date select 1 };
_day	  = if(count _this > 2) then { _this select 2 }else{ date select 2 };
_hour	  = if(count _this > 2) then { _this select 3 }else{ _this select 0 };
_min	  = if(count _this > 2) then { _this select 4 }else{ _this select 1 };

setDate [_year,_month,_day,_hour,_min];

if (true) exitWith {};