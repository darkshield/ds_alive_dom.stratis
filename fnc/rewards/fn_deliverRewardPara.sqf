/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/10/2016
* Description     : Handles reward selection and passing off to appropriate function for delivery.
* Example:
* ["_rewardDelivery","_rewardVeh","_rewardQTY"] call DS_fnc_DeliverRewardDrive;
*/

params ["_rewardDelivery","_rewardVeh","_rewardQTY"];

switch(_rewardDelivery) do {
  case "Blackfish": { _rewardDelivery = "B_T_VTOL_01_vehicle_F" };
};  

private ["_vehDesc","_delivDesc"]; 
_vehDesc = _rewardVeh call ISSE_Cfg_Vehicle_GetName;
_delivDesc = _rewardDelivery call ISSE_Cfg_Vehicle_GetName;
["HeadQuarters", format[localize "str_DS_reward_VEH_PARA",_rewardQTY,_vehDesc,_delivDesc]] remoteExec ["BIS_fnc_showSubtitle",[0,-2] select isDedicated];


for "_i" from 1 to _rewardQTY step 1 do {
  private ["_veh","_grp","_wp1","_wp2","_wp3","_cargoLoad"];
  _veh       = createVehicle [_rewardDelivery, [getMarkerPos "reward_paraplane_start" select 0,getMarkerPos "reward_paraplane_start" select 1,550], [], 50, "FLY"];
  createVehicleCrew _veh;
  _cargoload = createVehicle [_rewardVeh, [(getpos _veh )select 0,(getpos _veh) select 1,((getpos _veh) select 2) - 30], [], 0, "CAN_COLLIDE"];
  _veh setVehicleCargo _cargoLoad;
  _grp = group (driver _veh);

  _wp1 = _grp addWaypoint [getMarkerPos "reward_paraplane_drop",50];
  _wp1 setWaypointBehaviour "CARELESS";
  _wp1 setWaypointType "MOVE";
  _veh flyInHeight 500;

  waitUntil{Sleep 1; _veh distance2D getMarkerPos "reward_paraplane_drop" < 50};
  Sleep 2;
  _veh setvehicleCargo objNull;
  Sleep 1;  

  _wp2 = _grp addWaypoint [getMarkerPos "reward_paraplane_end",0];
  _wp2 setWaypointType "MOVE";
  _wp2 setWaypointBehaviour "CARELESS";
  _wp2 setWayPointCompletionRadius 250;

  waitUntil{Sleep 1; _veh distance2D getMarkerPos "reward_paraplane_end" < 250};
  {_veh deleteVehicleCrew _x} count crew _veh;
  deleteVehicle _veh;
}; 