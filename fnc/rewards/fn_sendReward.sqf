/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/10/2016
* Description     : Handles reward selection and passing off to appropriate function for delivery.
*
* Arguments: 
* 1: <STRING> Force a delivery type to spawn
*
* Return Value: <BOOL>
*
* Example:
* ["Drive"] call DS_fnc_sendReward; //Sends a car to be rewarded
*        [] call DS_fnc_sendReward; //Sends random reward
*/

params [["_forceType","",[""]]];

private ["_rewardVehArr","_rewardVeh","_rewardDelivery","_rewardQTY"];
private ["_spawnpos"];

_rewardVehArr   = DS_REWARD_VEHS call BIS_fnc_selectRandom;
_rewardDelivery = _rewardVehArr select 1;

if (_forceType != "") then {
  while {_rewardDelivery != _forceType} do {
    _rewardVehArr   = DS_REWARD_VEHS call BIS_fnc_selectRandom;
    _rewardDelivery = _rewardVehArr select 1;
  };
};

_rewardVeh      = _rewardVehArr select 0;
_rewardDelivery = _rewardVehArr select 1;
_rewardQTY      = _rewardVehArr select 2;
_needsAICrew    = _rewardVehArr select 3;

if (DS_PARAM_REWARDDELIVER) then {
  switch(_rewardDelivery) do {
    case "Drive"    : { [_rewardVeh,_rewardQTY]                 spawn DS_fnc_DeliverRewardDrive; };
    case "DriveBoat": { [_rewardVeh,_rewardQTY]                 spawn DS_fnc_DeliverRewardDriveBoat; };
    case "LandLZ"   : { [_rewardVeh,_rewardQTY]                 spawn DS_fnc_DeliverRewardLandLZ; };
    case "Huron"    : { [_rewardDelivery,_rewardVeh,_rewardQTY] spawn DS_fnc_DeliverRewardSlingDrop; };
    case "Taru"     : { [_rewardDelivery,_rewardVeh,_rewardQTY] spawn DS_fnc_DeliverRewardSlingDrop; };
    case "Blackfish": { [_rewardDelivery,_rewardVeh,_rewardQTY] spawn DS_fnc_DeliverRewardPara; };
    case "FIXEDWING": { 
      _spawnLoc = getMarkerPos "reward_instantfixed"; 
      _veh = createVehicle [_rewardVeh, _spawnLoc, [], 50, "NONE"]; 
      if (_needsAICrew) then {createVehicleCrew _veh};
      _veh setDir 200;
      ["HeadQuarters", format[localize "str_DS_reward_VEH_FIXEDWING",(_rewardVeh call ISSE_Cfg_Vehicle_GetName)]] remoteExec ["BIS_fnc_showSubtitle",[0,-2] select isDedicated];
    };
    case "CAS": { 
      _spawnPos = [(getMarkerPos "reward_instant"), 0, 50, 15, 0, 100, 0] call BIS_fnc_findSafePos; //
      ["CAS",[_spawnPos,65,_rewardVeh,format["Phantom %1",DS_CAS_COUNT],"",""]] Call ALiVE_fnc_combatSupportAdd;
      ["HeadQuarters", format[localize "str_DS_reward_VEH_COMBATSUPPORT",(_rewardVeh call ISSE_Cfg_Vehicle_GetName), format["Phantom %1",DS_CAS_COUNT]]] remoteExec ["BIS_fnc_showSubtitle",[0,-2] select isDedicated];
      DS_CAS_COUNT = DS_CAS_COUNT + 1; publicVariable "DS_CAS_COUNT";
    };
    case "ARTY": { 
      ["ARTILLERY",[getMarkerPos "reward_instant",65,_rewardVeh,format["Avalanche %1",DS_ARTY_COUNT],"",""]] Call ALiVE_fnc_combatSupportAdd;
      ["HeadQuarters", format[localize "str_DS_reward_VEH_COMBATSUPPORT",(_rewardVeh call ISSE_Cfg_Vehicle_GetName), format["Avalanche %1",DS_ARTY_COUNT]]] remoteExec ["BIS_fnc_showSubtitle",[0,-2] select isDedicated];
      DS_ARTY_COUNT = DS_ARTY_COUNT + 1; publicVariable "DS_ARTY_COUNT";
    };
  }; 
} else {
  private ["_spawnLoc","_veh"];
  switch(_rewardDelivery) do {
    case "DriveBoat": { 
      _spawnLoc = getMarkerPos "reward_instantboat"; 
      _veh = createVehicle [_rewardVeh, _spawnLoc, [], 50, "NONE"]; 
      ["HeadQuarters", format[localize "str_DS_reward_VEH_INSTANT",(_rewardVeh call ISSE_Cfg_Vehicle_GetName)]] remoteExec ["BIS_fnc_showSubtitle",[0,-2] select isDedicated];
    };
    case "FIXEDWING": { 
      _spawnLoc = getMarkerPos "reward_instantfixed";     
      _veh = createVehicle [_rewardVeh, _spawnLoc, [], 50, "NONE"]; 
      _veh setDir 200; 
      ["HeadQuarters", format[localize "str_DS_reward_VEH_INSTANT",(_rewardVeh call ISSE_Cfg_Vehicle_GetName)]] remoteExec ["BIS_fnc_showSubtitle",[0,-2] select isDedicated];
    };
    case "LandLZ"   : { [_rewardVeh,_rewardQTY] spawn DS_fnc_DeliverRewardLandLZ; };
    case "CAS": { 
      _spawnPos = [(getMarkerPos "reward_instant"), 0, 50, 15, 0, 100, 0] call BIS_fnc_findSafePos; //
      ["CAS",[_spawnPos,65,_rewardVeh,format["Phantom %1",DS_CAS_COUNT],"",""]] Call ALiVE_fnc_combatSupportAdd;
      ["HeadQuarters", format[localize "str_DS_reward_VEH_COMBATSUPPORT",(_rewardVeh call ISSE_Cfg_Vehicle_GetName), format["Phantom %1",DS_CAS_COUNT]]] remoteExec ["BIS_fnc_showSubtitle",[0,-2] select isDedicated];
      DS_CAS_COUNT = DS_CAS_COUNT + 1; publicVariable "DS_CAS_COUNT";
    };
    case "ARTY": { 
      ["ARTILLERY",[getMarkerPos "reward_instant",65,_rewardVeh,format["Avalanche %1",DS_ARTY_COUNT],"",""]] Call ALiVE_fnc_combatSupportAdd;
      ["HeadQuarters", format[localize "str_DS_reward_VEH_COMBATSUPPORT",(_rewardVeh call ISSE_Cfg_Vehicle_GetName), format["Avalanche %1",DS_ARTY_COUNT]]] remoteExec ["BIS_fnc_showSubtitle",[0,-2] select isDedicated];
      DS_ARTY_COUNT = DS_ARTY_COUNT + 1; publicVariable "DS_ARTY_COUNT";
    };
    default           { 
      _spawnLoc = getMarkerPos "reward_instant";     
      _veh = createVehicle [_rewardVeh, _spawnLoc, [], 50, "NONE"]; 
      ["HeadQuarters", format[localize "str_DS_reward_VEH_INSTANT",(_rewardVeh call ISSE_Cfg_Vehicle_GetName)]] remoteExec ["BIS_fnc_showSubtitle",[0,-2] select isDedicated];
    }; 
  };
  if (_needsAICrew) then {createVehicleCrew _veh};
};
