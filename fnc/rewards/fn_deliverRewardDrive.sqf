/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/10/2016
* Description     : Handles reward selection and passing off to appropriate function for delivery.
* Example:
* [_rewardVeh,_rewardQTY] call DS_fnc_DeliverRewardDrive;
* **LOCALIZED**
*/
params ["_rewardVeh","_rewardQTY"];
private ["_vehDesc"]; 

_vehDesc = _rewardVeh call ISSE_Cfg_Vehicle_GetName;

["HeadQuarters", format[localize "str_DS_reward_VEH_DRIVE",_rewardQTY,_vehDesc]] remoteExec ["BIS_fnc_showSubtitle",[0,-2] select isDedicated];


for "_i" from 1 to _rewardQTY step 1 do {
  private ["_veh","_grp","_wp1","_wp2"];
  _veh = createVehicle [_rewardVeh, getMarkerPos "reward_drive_start", [], 0, "NONE"];
  createVehicleCrew _veh;
  _grp = group (driver _veh);

  _wp1 = _grp addWaypoint [getMarkerPos "reward_drive_end",0];
  _wp1 setWaypointType "GETOUT";
  _wp1 setWaypointBehaviour "CARELESS";
  _wp1 setWayPointCompletionRadius 50;

  _wp2 = _grp addWaypoint [getMarkerPos "reward_crew_despawn",0];
  _wp2 setWaypointType "MOVE";
  _wp2 setWaypointSpeed "LIMITED";
  _wp2 setWaypointBehaviour "CARELESS";
  _wp2 setWayPointCompletionRadius 50;
  _wp2 setWayPointStatements ["true", "{deleteVehicle _x} forEach thislist;"]; 
};