/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/10/2016
* Description     : Handles reward selection and passing off to appropriate function for delivery.
* Example:
* [_rewardVeh,_rewardQTY] call DS_fnc_DeliverRewardDrive;
*/

params ["_rewardVeh","_rewardQTY"];
private ["_vehDesc"]; 
_vehDesc = _rewardVeh call ISSE_Cfg_Vehicle_GetName;
["HeadQuarters", format[localize "str_DS_reward_VEH_LZ",_rewardQTY,_vehDesc]] remoteExec ["BIS_fnc_showSubtitle",[0,-2] select isDedicated];
for "_i" from 1 to _rewardQTY step 1 do {
  private ["_veh","_grp","_wp1","_wp2","_wp3"];
  _veh = createVehicle [_rewardVeh, [(getMarkerPos "reward_lz_start") select 0,(getMarkerPos "reward_lz_start") select 1,100], [], 250, "FLY"];
  createVehicleCrew _veh;
  _grp = group (driver _veh);

  _wp1 = _grp addWaypoint [getMarkerPos "reward_lz_end",0];
  _wp1 setWaypointBehaviour "CARELESS";
  _wp1 setWaypointType "TR UNLOAD";

  _wp2 = _grp addWaypoint [getMarkerPos "reward_lz_end",0];
  _wp2 setWaypointType "GETOUT";
  _wp2 setWaypointBehaviour "CARELESS";
  _wp2 synchronizeWayPoint [_wp1];

  _wp3 = _grp addWaypoint [getMarkerPos "reward_crew_despawn",0];
  _wp3 setWaypointType "MOVE";
  _wp3 setWaypointSpeed "LIMITED";
  _wp3 setWaypointBehaviour "CARELESS";
  _wp3 setWayPointCompletionRadius 50;
  _wp3 setWayPointStatements ["true", "{deleteVehicle _x} forEach thislist;"]; 

};