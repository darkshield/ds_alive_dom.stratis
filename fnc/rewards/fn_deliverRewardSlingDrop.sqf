/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/10/2016
* Description     : Handles reward selection and passing off to appropriate function for delivery.
* Example:
* ["_rewardDelivery","_rewardVeh","_rewardQTY"] call DS_fnc_DeliverRewardDrive;
*/

params ["_rewardDelivery","_rewardVeh","_rewardQTY"];

switch(_rewardDelivery) do {
  case "Huron": { _rewardDelivery = "B_Heli_Transport_03_unarmed_F" };
};  


private ["_vehDesc","_delivDesc"]; 
_vehDesc = _rewardVeh call ISSE_Cfg_Vehicle_GetName;
_delivDesc = _rewardDelivery call ISSE_Cfg_Vehicle_GetName;
["HeadQuarters", format[localize "str_DS_reward_VEH_SLING",_vehDesc,_delivDesc]] remoteExec ["BIS_fnc_showSubtitle",[0,-2] select isDedicated];

for "_i" from 1 to _rewardQTY step 1 do {
  private ["_veh","_grp","_wp1","_wp2","_wp3","_slingLoad"];
  _veh       = createVehicle [_rewardDelivery, [getMarkerPos "reward_sling_start" select 0,getMarkerPos "reward_sling_start" select 1,100], [], 50, "FLY"];
  createVehicleCrew _veh;
  _slingload = createVehicle [_rewardVeh, [(getpos _veh )select 0,(getpos _veh) select 1,((getpos _veh) select 2) - 20], [], 0, "CAN_COLLIDE"];
  _veh setSlingLoad _slingLoad;
  _grp = group (driver _veh);

  _wp1 = _grp addWaypoint [getMarkerPos "reward_sling_drop",1];
  _wp1 setWaypointBehaviour "CARELESS";
  _wp1 setWaypointType "MOVE";
  _veh flyInHeight 30;

  waitUntil{sleep 1; unitReady _veh};
  _veh disableAI "MOVE";
  Sleep 3;
  waitUntil {sleep 0.25; _veh setVelocity [0,0,-5]; getPos _veh select 2 < 25;};
  Sleep 2;
  _veh setSlingLoad objNull;
  Sleep 2;
  _veh enableAI "MOVE";
  Sleep 1;  

  _wp2 = _grp addWaypoint [getMarkerPos "reward_sling_end",0];
  _wp2 setWaypointType "MOVE";
  _wp2 setWaypointBehaviour "CARELESS";
  _wp2 setWayPointCompletionRadius 250;

  waitUntil{Sleep 1; _veh distance2D getMarkerPos "reward_sling_end" < 250};
  {_veh deleteVehicleCrew _x} count crew _veh;
  deleteVehicle _veh;
}; 