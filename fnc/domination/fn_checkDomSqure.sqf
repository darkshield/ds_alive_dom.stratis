/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/11/2016
* Description     : Used to check the square on each enemy unit's death and update accordingly.
*/
private ["_domMarkers"]; _domMarkers = [] call DS_fnc_returnDomMarkers;
private ["_unitSide","_nearObjs","_countInSquare"];
private ["_unitsMarker"];

_unitSide = side _unit;
_countInSquare = 0;

_domMarkers = _domMarkers select 2; // Red Markers only.

{
  if ([_x,_this] call BIS_fnc_InTrigger) exitWith {
    _unitsMarker = _x;
    //Check to see if there are any more enemys in square.
    _nearEnemy = nearestObjects [_unit, ["Car","Tank","Man"], 71];  // 70.71 is the hypotenuse of a 50x50 square cut into two triangles from a far corner.
      { 
        if ((side _x == enemySide) && (alive _x) && ([_unitsMarker,_x] call BIS_fnc_InTrigger)) then { 
          _countInSquare = _countInSquare + 1;
          //This is an enemy nearby of the same side and he's still within our square!
        };
      } forEach _nearEnemy;
  };
} forEach _domMarkers;

//Now we have a count of units within the square. Change the color if we've cleared the squre.
if (_countInSquare == 0 ) then { _unitsMarker setMarkerColor "ColorGreen" } else { _unitsMarker setMarkerColor "ColorRed" }