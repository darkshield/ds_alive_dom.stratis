/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/11/2016
* Description     : Grabs all "dom_#" markers on the map returns them seperated by color.
*
* Return Value: <ARRAY> Domination Markers and Seperated by CurrentColor
*/
private ["_domMarkers"];    _domMarkers    = [];
private ["_domMarkersGre"]; _domMarkersGre = [];
private ["_domMarkersRed"]; _domMarkersRed = [];
private ["_domMarkersUNK"]; _domMarkersUNK = [];
private ["_result"];        _result        = [];

{
  private ["_mrk"];
  _mrk = toArray _x;
  _mrk resize 4;
  if ((toString _mrk) == "dom_") then { _domMarkers pushback _x; };
} forEach allMapMarkers;


{
  switch (getMarkerColor _x) do {
    case "ColorGreen": { _domMarkersGre pushBack _x;};
    case "ColorRed"  : { _domMarkersRed pushBack _x;};
    default            { _domMarkersUNK pushBack _x;};
  };
} forEach _domMarkers;


_result = [_domMarkers,_domMarkersGre,_domMarkersRed,_domMarkersUNK];

_result