/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/11/2016
* Description     : Main Domination Script that checks squares and will eventually end mission.
*
* Arguments: 
* 1: <NUMBER> Quick Check - Run through the entire script only once to force an update.
*
* Return Value: NONE
*
* Usage: [1] spawn DS_fnc_HeartBeatSquarecheck; // will run only once
* Usage: [ ] spawn DS_fnc_HeartBeatSquarecheck; // will run endlessly with either 30/60 second sleep
*
*/
if !(DS_SRV || DS_DEDICATED) exitWith {};

params [["_quickCheck",0 ,[0 ]]];

private ["_domMarkers"];    _domMarkers = [] call DS_fnc_returnDomMarkers;
private ["_countAllMrks"];  _countAllMrks = count (_domMarkers select 0);
private ["_domMarkersGre"];
private ["_nearObj","_curMrk"];
private ["_justPlayers"];

while {DS_CAPPED_TILES != _countAllMrks} do {
  _justPlayers = allPlayers;
  if (_quickCheck == 0 ) then {
    if (count _justPlayers > 25) then {sleep 15;} else { sleep 5;};
  };

  [] spawn DS_fnc_alivemodforcepool;

  _domMarkers = [] call DS_fnc_returnDomMarkers;
  _domMarkers = _domMarkers select 0;
  _domMarkersGre = _domMarkers select 1;
  {
    _curMrk = _x;
    _nearObj = nearestObjects [getMarkerPos _x, ["Car","Tank","Man"], 100];
    {

      if ([_curMrk,_x] call BIS_fnc_InTrigger && ((side _x) == DS_ENEMY_SIDE) && (alive _x)) then {
        //Found an Enemy Man/Tank/Car in the current marker - set it's color back to red.
        _curMrk setMarkerColor "ColorRed";
      };

      //ALIVE VIRTUALIZATION
      if ((count ([getMarkerPos _curMrk, 70 , ["EAST","entity"]] call ALIVE_fnc_getNearProfiles)) > 0) then {
        _curMrk setMarkerColor "ColorRed";
      };
      if ((count ([getMarkerPos _curMrk, 100, ["EAST","vehicle"]] call ALIVE_fnc_getNearProfiles)) > 0) then {
        _curMrk setMarkerColor "ColorRed";
      };

      { //check each player to see if he's in that square
        if ([_curMrk,_x] call BIS_fnc_InTrigger) then {
          _curMrk setMarkerColor "ColorGreen";
        };
      } forEach _justPlayers;

    } forEach _nearObj;
  } forEach _domMarkers;  

  DS_CAPPED_TILES = count (call DS_fnc_returnDomMarkers select 1);
  if (_quickCheck != 0) exitWith {};
};

if (DS_CAPPED_TILES == _countAllMrks) then { //_countAllMrks

//RemoteExecCall check
  ["end1",true,true,true,false] call BIS_fnc_endMission;
};