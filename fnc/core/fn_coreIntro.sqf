/***
 *    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
 *    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
 *    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
 *    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
 *    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
 *    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
 * Original Author : Wyste
 * Last Editor     : Wyste
 * Last Edited     : 09/09/2016
 * Description     : Introduction cinematic when entering the mission from lobby.
 */
if(isDedicated) exitWith{};

if (hasInterface) then {
 	[] spawn {
		waitUntil {!isNull player};
	    titleText ["Wyste & BBrown of DarkShield present..", "BLACK IN",9999];
		sleep 5;
		titleText ["O P E R A T I O N   D A R K S H I E L D   -   D O M I N A T I O N   [ A L I V E ]", "BLACK IN",10];
        sleep 15;
    };

	[["Stratis Airfield"],
	[(str(date select 1) + "-" + str(date select 2) + "-" + str(date select 0)),1,5],
	["Operation DarkShield",1,1,4]] spawn BIS_fnc_EXP_camp_SITREP;
    waituntil {!isnull player};
};