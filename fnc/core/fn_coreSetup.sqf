/***
 *    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
 *    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
 *    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
 *    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
 *    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
 *    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
 * Original Author : Wyste
 * Last Editor     : Wyste
 * Last Edited     : 09/10/2016
 * Description     : Game Mode Configuration / Setup Script.
 */
 //    GENERAL OPTIONS

0 fadeRadio 0;
enableRadio false;
enableSentences false;

 //    PARAMETER IMPORT / ASSIGNING TO VARIABLES
private["_pname","_pval","_pcode","_var"];
{ 
	_pname	= configName ((missionConfigFile >> "Params") select _ForEachIndex);
	_pCode	= getText (missionConfigFile >> "Params" >> _pname >> "code");
	_pval	= paramsArray select _ForEachIndex;
	_var	= format[_pCode, _pval];
	if( _pcode != "" ) then {	call compileFinal _var;	};
} forEach paramsArray;


//----- Diary (Map Briefing) Setup

switch (WorldName) do {
  case "Stratis": {
		player createDiaryRecord ["Diary", ["Base", "Friendly forces base is located at <marker name='BLUFOR_1'>Stratis Airfield</marker>"]];		
	};
  case "Altis"  : {};
  case "Tanoa"  : {};
};

player createDiaryRecord ["Diary", ["Mission Completion", "Mission is complete when BLUFOR Forces completely capture all red squares on map simultaneously."]];
player createDiaryRecord ["Diary", ["SideMissions", "Side Missions, when completed successfully, will award heavy armor, air assets, or other beneficial vehicles to the team. Careful, as these are non-respawning vehicles! Take care of them."]];
player createDiaryRecord ["Diary", ["Repairing Vehicles", "Any player can repair vehicles - just need a Toolkit."]];
player createDiaryRecord ["Diary", ["Disarming Mines", "Any player can disarm mines - just need a Toolkit."]];
player createDiaryRecord ["Diary", ["Found a bug?", "Please submit it to our issue tracker on Bitbucket: https://bitbucket.org/darkshield/ds_alive_dom.stratis"]];
player createDiaryRecord ["Diary", ["Created by DarkShield", "This mission was created by the owners of DarkShield, a multi-genre gaming group. Please visit our website at http://www.darkshield.org."]];