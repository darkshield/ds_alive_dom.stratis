/***
 *    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
 *    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
 *    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
 *    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
 *    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
 *    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
 * Original Author : BBrown
 * Last Editor     : BBrown
 * Last Edited     : 09/11/2016
 * Description     : Handles additional code execution upon player respawn
 */

/* START WYSTE 9/14/2016 - Checks PlayerUIDS against List defined in UIDS.hpp. */
  if (isDedicated && !hasInterface) exitWith {};

  private ["_reserved_units", "_reserved_uids", "_uid"];

  _reserved_uids = [
  "76561198007455961" /* WYSTE    */,
  "76561198031182950" /* BBROWN   */,
  "" /* PLAYERNAME */,
  "" /* PLAYERNAME */,
  "" /* PLAYERNAME */,
  "" /* PLAYERNAME */,
  "" /* PLAYERNAME */    // Last Line! No comma.
  ];

  waitUntil {!isNull player};
  waitUntil {(vehicle player) == player};
  waitUntil {(getPlayerUID player) != ""};

  // Variable Name of the Player Character to be restricted. //
  _reserved_units = [];
  if !(isNil "RESERVED1") then {_reserved_units pushBack "RESERVED1"};
  if !(isNil "RESERVED2") then {_reserved_units pushBack "RESERVED2"};
  if !(isNil "RESERVED3") then {_reserved_units pushBack "RESERVED3"};
  if !(isNil "RESERVED4") then {_reserved_units pushBack "RESERVED4"};
  if !(isNil "RESERVED5") then {_reserved_units pushBack "RESERVED5"};

  // Stores the connecting player's UID //
  _uid = getPlayerUID player;

  if ((player in _reserved_units)&& !(_uid in _reserved_uids)) then {
    titleText ["", "BLACK OUT"];
    disableUserInput true;
    hint "You are in a reserved slot! You will be kicked to the lobby in 10 seconds!";
    sleep 5;
    hint "You are in a reserved slot! You will be kicked to the lobby in 5 seconds!";
    sleep 5;
    titleText ["", "BLACK IN"];
    disableUserInput false;
    failMission "endRsv";
  };
/* END   WYSTE 9/14/2016 - Checks PlayerUIDS against List defined in UIDS.hpp. */


/* START WYSTE 9/14/2016 - Enables anyone with a toolkit to disarm mines within 3m. */
[player,                                                               
localize "str_DS_DisarmMine",                                          
"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",        
"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",        
"  ((player distance (nearestObject [player,'TimeBombCore']))<3)&&('ToolKit' in (items player))
|| ((player distance (nearestObject [player,'MineGeneric']))<3)&&('ToolKit' in (items player))
|| ((player distance (nearestObject [player,'MineBase']))<3)&&('ToolKit' in (items player))",
                                      
"  ((player distance (nearestObject [player,'TimeBombCore']))<3)&&('ToolKit' in (items player))
|| ((player distance (nearestObject [player,'MineGeneric']))<3)&&('ToolKit' in (items player))
|| ((player distance (nearestObject [player,'MineBase']))<3)&&('ToolKit' in (items player))",                                             
{},{},{_this spawn DS_fnc_disarmMine},{},[],5,0,false,false                                         
] remoteExecCall ["BIS_fnc_holdActionAdd",[0,-2] select isDedicated,player]; 
/* END   WYSTE 9/14/2016 - Enables anyone with a toolkit to disarm mines within 3m. */

/* START WYSTE 9/18/2016 - Enables anyone with a toolkit to repair vehicles on-tick*/
[player,                                                               
localize "str_DS_RepairNearbyVeh",                                          
"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",        
"\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",        
"  (((player distance (nearestObject [player,'Car']))<5)  && (vehicle player == player) &&('ToolKit' in (items player)) && (damage (nearestObject [player,'Car' ]))<1 && (damage (nearestObject [player,'Car' ]))>0) 
|| (((player distance (nearestObject [player,'Tank']))<5) && (vehicle player == player) &&('ToolKit' in (items player)) && (damage (nearestObject [player,'Tank']))<1 && (damage (nearestObject [player,'Tank']))>0)
|| (((player distance (nearestObject [player,'Air']))<5)  && (vehicle player == player) &&('ToolKit' in (items player)) && (damage (nearestObject [player,'Air' ]))<1 && (damage (nearestObject [player,'Air' ]))>0)
|| (((player distance (nearestObject [player,'Ship']))<5) && (vehicle player == player) &&('ToolKit' in (items player)) && (damage (nearestObject [player,'Ship']))<1 && (damage (nearestObject [player,'Ship']))>0)",                                      

"  (((player distance (nearestObject [player,'Car']))<5)  && (vehicle player == player) &&('ToolKit' in (items player)) && (damage (nearestObject [player,'Car' ]))<1 && (damage (nearestObject [player,'Car' ]))>0)
|| (((player distance (nearestObject [player,'Tank']))<5) && (vehicle player == player) &&('ToolKit' in (items player)) && (damage (nearestObject [player,'Tank']))<1 && (damage (nearestObject [player,'Tank']))>0)
|| (((player distance (nearestObject [player,'Air']))<5)  && (vehicle player == player) &&('ToolKit' in (items player)) && (damage (nearestObject [player,'Air' ]))<1 && (damage (nearestObject [player,'Air' ]))>0)
|| (((player distance (nearestObject [player,'Ship']))<5) && (vehicle player == player) &&('ToolKit' in (items player)) && (damage (nearestObject [player,'Ship']))<1 && (damage (nearestObject [player,'Ship']))>0)",                                          
{},
{_this call DS_fnc_repairVehicle},//tick}
{},{},[],24,0,false,false                                         
] remoteExecCall ["BIS_fnc_holdActionAdd",[0,-2] select isDedicated,player]; 
/* END   WYSTE 9/18/2016 - Enables anyone with a toolkit to repair vehicles on-tick*/
