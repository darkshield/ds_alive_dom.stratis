/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/10/2016
* Description     : Handles reward definition and delivery possibilities.
╔══════════════════════════════════════════════════════╗ ╔═════════════════════════════════════════════════╗
║          V44 Blackfish VEH Cargo Capacities          ║ ║          Y-32 Xi'an VEH Cargo Capacities        ║
╠═══════════════╦═════════╦════════════════╦═══════════╣ ╠════════════╦═════════════╦═════════════╦════════╣ 
║ NATO          ║ CSAT    ║ Civillian      ║ AAF       ║ ║ NATO       ║ CSAT        ║ Civillian   ║ AAF    ║  
╠═══════════════╬═════════╬════════════════╬═══════════╣ ╠════════════╬═════════════╬═════════════╬════════╣  
║ Hunter-1      ║ Marid-1 ║ Hatchback-2    ║ Strider-1 ║ ║ Prowler-1  ║ Qilin-1     ║ Offroad-1   ║ <None> ║  
║ Marshal-1     ║ Qilin-1 ║ MB 4WD-2       ║ Zamak-1   ║ ║            ║ UGV Drone-1 ║ SUV-1       ║        ║  
║ Prowler-2     ║         ║ Offroad-1      ║           ║ ║            ║             ║ MB 4WD-1    ║        ║  
║ HEMTT(Open)-1 ║         ║ Truck-1        ║           ║ ║            ║             ║ QUAD BIKE-2 ║        ║
║ UGV Drone-2   ║         ║ SUV-1          ║           ║ ║            ║             ║ HATCHBACK-1 ║        ║
║               ║         ║ Quad-4         ║           ║ ║            ║             ║ JETSKI-1    ║        ║
║               ║         ║ Jetski-2       ║           ║ ║            ║             ║             ║        ║
║               ║         ║ Assualt Boat-1 ║           ║ ║            ║             ║             ║        ║
╚═══════════════╩═════════╩════════════════╩═══════════╝ ╚════════════╩═════════════╩═════════════╩════════╝

╔══════════════════════════════════════╗ ╔═════════════════════════════════════════╗
║ ==========Vehicle Weights=========== ║ ║ =========Sling Load Capacities========= ║
╠══════════════════════════════════════╣ ╠═════════════════════════════════════════╣
║ Kart:                         150 kg ║ ║ MH-9 Hummingbird: 0.50 tonnes (  500kg) ║
║ Quad bike:                    281 kg ║ ║ PO-30 Orca:       02.0 tonnes ( 2000kg) ║
║ Hatchback:                   1090 kg ║ ║ WY-55 Hellcat:    02.0 tonnes ( 2000kg) ║
║ Hatchback (sport):           1090 kg ║ ║ UH-80 Ghost Hawk: 04.0 tonnes ( 4000kg) ║
║ SUV:                         1600 kg ║ ║ CH-49 Mohawk:     04.0 tonnes ( 4000kg) ║
║ Offroad:                     1615 kg ║ ║ Huron:            10.0 tonnes (10000kg) ║
║ Offroad (armed):             1615 kg ║ ║ Taru:             12.0 tonnes (12000kg) ║
║ Truck:                       2401 kg ║ ╚═════════════════════════════════════════╝ 
║ Truck Boxer:                 2510 kg ║
║ Hunter:                      8307 kg ║
║ Ifrit:                       8392 kg ║
║ Zamak Transport (covered):   9376 kg ║   Use the tables defined here to selecting
║ Strider:                     9700 kg ║   an appropriate delivery package for
║ HEMTT                       10843 kg ║   cinematic reward delvery parameter.
║ Zamak Transport:            10868 kg ║
║ HEMTT Transport:            10943 kg ║
║ HEMTT Transport Covered:    11027 kg ║
║ Assault boat:                 565 kg ║
║ Rescue boat:                  565 kg ║
║ SDV:                         2600 kg ║
╚══════════════════════════════════════╝    */

// Format: ["classname","deliverypackage",QTY]
DS_REWARD_VEHS = [
  ["B_APC_Tracked_01_AA_F",          "Drive",1,false],
  ["B_APC_Tracked_01_rcws_F",        "Drive",1,false],
  ["B_MBT_01_arty_F",                "Drive",1,false],
  ["B_MBT_01_mlrs_F",                "Drive",1,false],
  ["B_MBT_01_cannon_F",              "Drive",1,false],
  ["B_MBT_01_TUSK_F",                "Drive",1,false],
  ["B_Boat_Armed_01_minigun_F",  "DriveBoat",2,false],
  ["B_MRAP_01_gmg_F",                "Huron",2,false],
//["B_MRAP_01_hmg_F",                "Huron",2,false],
  ["B_Heli_Attack_01_F",            "LandLZ",1,false],
  ["B_Heli_Light_01_armed_F",       "LandLZ",1,false],
  ["B_UGV_01_rcws_F",            "Blackfish",2,true ],
  ["B_APC_Wheeled_01_cannon_F",  "BlackFish",2,false],
  ["B_Heli_Light_01_armed_F",          "CAS",1,false],
//["B_MBT_01_arty_F",                 "ARTY",1,false],
//["B_MBT_01_mlrs_F",                 "ARTY",1,false],
  ["B_Heli_Attack_01_F",               "CAS",1,false] 
  ];

DS_REWARD_APEX_VEHS = [["B_LSV_01_armed_F","BlackFish",1]];

//Mission Parameter Check - Apex Vehicle Rewards Allowed
if (DS_PARAM_ALLOWAPEXREWARD) then { DS_REWARD_VEHS = DS_REWARD_VEHS + DS_REWARD_APEX_VEHS; };
//Mission Parameter Check - Fixed Wing Rewards Allowed
if (DS_PARAM_ALLOWFIXEDWINGREWARD) then {
  DS_REWARD_VEHS pushBack ["B_Plane_CAS_01_F","FIXEDWING",1,false];
  DS_REWARD_VEHS pushBack ["B_UAV_02_CAS_F",  "FIXEDWING",1,true];
};