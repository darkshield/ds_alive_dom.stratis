/***
 *    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
 *    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
 *    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
 *    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
 *    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
 *    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
 * Original Author : Wyste
 * Last Editor     : Wyste
 * Last Edited     : 09/10/2016
 * Description     : Essential Arma3 Script
*/

class DS_param_missionparamstitle {
  title="===---===---===---=== Mission Environment";
  values[]={0};
  texts[]={""};
  default=0;
  code="";
};

class DS_param_missionhour {
  title = "Mission Start Time";
  values[]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};
  texts[]={"0000H","0100H","0200H","0300H","0400H","0500H","0600H","0700H","0800H","0900H","1000H","1100H","1200H","1300H","1400H","1500H","1600H","1700H","1800H","1900H","2000H","2100H","2200H","2300H"};
  default=12;
  code="DS_PARAM_MISSIONSTARTTIME = %1";
};

class DS_param_accTime {
  title = "Time Accelleration";
  values[]={1,2,3,4,5,6};
  texts[]={"x1 (24hrs = 24hrs)","x2 (24hrs = 12hrs)","x3 (24hrs = 8hrs)","x4 (24hrs = 6hrs)","x5 (24hrs = 4.8hrs)","x6 (24hrs = 4hrs)"};
  default=1;
  code="setTimeMultiplier %1";
};
/*
class DS_param_missionparamstitle2 {
  title="===---===---===---=== Equipment / Role Options";
  values[]={0};
  texts[]={""};
  default=0;
  code="";
};

class DS_param_equipmentProgression {
  title = "Equipment Progression";
  values[]={"Full","SideMissionUnlock"};
  texts[]={"Full Arsenal Always Avail","SideMission Unlocks"};
  default="Full";
  code="DS_PARAM_EQPTPROGRESSION = %1";
}

class DS_param_roleRestriction {
  title = "Aresenal Equipment Role Restricted";
  values[]={1,0};
  texts[]={"True","False"};
  default=1;
  code="DS_PARAM_EQPTROLERESTRICT = if(%1 > 0)then{true}else{false};";
}
*/
class DS_param_missionparamstitle3 {
  title="===---===---===---=== Side Missions";
  values[]={0};
  texts[]={""};
  default=0;
  code="";
};

class DS_param_OneSideMissionChance {
  title = "Probability to Keep 1 Side Mission Active";
  values[]={10,25,50,75,100};
  texts[]={"10%","25%","50%","75%","100%"};
  default=100;
  code="DS_PARAM_ONESIDEMISSIONCHANCE = %1";
};

class DS_param_cinematicRewardDelivery {
  title = "Cinematic Delivery of Rewards";
  values[]={1,0};
  texts[]={"True","False"};
  default=1;
  code="DS_PARAM_REWARDDELIVER = if(%1 > 0)then{true}else{false};";
}

class DS_param_sideMissionFixedWingAllowed {
  title = "Allow Fixed Wing Rewards";
  values[]={1,0};
  texts[]={"True","False"};
  default=1;
  code="DS_PARAM_ALLOWFIXEDWINGREWARD = if(%1 > 0)then{true}else{false};";
}

class DS_param_sideMissionApexAllowed {
  title = "Allow Apex Vehicles as Rewards";
  values[]={1,0};
  texts[]={"True","False"};
  default=1;
  code="DS_PARAM_ALLOWAPEXREWARD = if(%1 > 0)then{true}else{false};";
}

class DS_param_missionparamstitle4 {
  title="===---===---===---=== Intel Gathering";
  values[]={0};
  texts[]={""};
  default=0;
  code="";
};

class DS_param_intelDropChance {
  title = "Intel Drop Chance off Enemies";
  values[]={5,10,15,20,25};
  texts[]={"5%","10%","15%","25%"};
  default=20;
  code="DS_PARAM_INTELDROPCHANCE = %1";
};
