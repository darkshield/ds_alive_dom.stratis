/***
*    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
*    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
*    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
*    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
*    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
*    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
* Original Author : Wyste
* Last Editor     : Wyste
* Last Edited     : 09/10/2016
* Description     : Handles intel classname definition and chance rates.
*/

DS_INTEL_INF_NEXT_ENEMY_REVEAL = 40; // 30%
DS_INTEL_INF_SIDEMISSION       = 15;  // 05%
DS_INTEL_CACHE_SPAWN_RATE      = 50; // 50%
DS_INTEL_CACHE_2_SPAWN_RATE    = 10; // 10%
DS_INTEL_CACHE_SIDEMISSION     = 25; // 25%

DS_ROOT_CACHE_CLASSNAME = "Land_WoodenTable_large_F";

DS_CACHE_COMP_CLASSES =["intelcachetable1"];

DS_INTELDROP_CLASSES = ["Land_HandyCam_F","Land_SatellitePhone_F","Land_Suitcase_F",
                        "Land_PortableLongRangeRadio_F","Land_File1_F","Land_File2_F",
                        "Land_FilePhotos_F","Land_Map_F","Land_Map_unfolded_F",
                        "Land_MobilePhone_smart_F","Land_MobilePhone_old_F"];