/***
 *    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
 *    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
 *    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
 *    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
 *    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
 *    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
 * Original Author : Wyste
 * Last Editor     : Wyste
 * Last Edited     : 09/09/2016
 * Description     : Function Definition
 */
class DS
{
  tag = "DS";

  class core
  {
    file = "fnc\core";
    class coreSetup{};
    class coreIntro{};
  };

  class debug
  {
    file = "fnc\debug";
    class debugSetup{};
  };

  class environment
  {
    file = "fnc\environment";
    class setTime{};
  };
  class equipment
  {
    file = "fnc\equipment";
    class loadoutSetup{};
    class initializeArsenal{};
    class repairvehicle{};
    class disarmMine{};
  };
  
  class rewards
  {
    file = "fnc\rewards";
    class sendReward{};
    class deliverRewardDrive{};
    class deliverRewardDriveBoat{};
    class deliverRewardLandLZ{};
    class deliverRewardSlingDrop{};
    class deliverRewardPara{};
  };

  class eventhandlers
  {
    file = "fnc\eventhandlers";
    class InfantryKilledIntelEH{};
    class InfantryKilledDomSquare{};
  };

  class intel
  {
    file = "fnc\intel";
    class intelDrop{};
    class intelGatherComplete{};
    class spawnCache{};
  };

  class domination
  {
    file = "fnc\domination";
    class returnDomMarkers{};
    class checkDomSqure{};
    class HeartBeatSquareCheck{};
  };

  class locations
  {
    file = "fnc\locations";
    class findLocation{};
    class findBuildings{};
    class getEnterableBldgs{};
    class findRandBldgPos{};
    class getSecludedLocation{};
  };

  class sidemissions
  {
    file = "fnc\sidemissions";
    class sm_start_wepcache{};
    class sm_end_wepcache{};
    class sm_start_killHVT{};
    class sm_end_killHVT{};
    class sm_start_mines{};
    class sm_end_mines{};
    class sm_start_convoy{};
    class sm_end_convoy{};
    class sm_start_helicrash{};
    class sm_end_helicrash{};
    class sm_start_hostage{};
    class sm_end_hostage{};
    class selectRandomsideMission{};
    class AddMarker{};
    class HeartbeatSideMissionCheck{};
  };

  class hooks
  {
    file = "fnc\hooks";
    class aliveaddobjtoside{};
    class alivehaltprofilesenrouteto{};
    class alivenearestobjtoignoring{};
    class aliverandompointnear{};
    class aliveremoveobjectivefromsides{};
    class alivemodforcepool{};
  };
};