/***
 *    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
 *    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
 *    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
 *    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
 *    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
 *    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
 * Original Author : BBrown
 * Last Editor     : BBrown
 * Last Edited     : 09/12/2016
 * Description     : Contains the arrays of equipment types for the Arsenal whitelist
***/

//Weapon Cargo for virtual Arsenal
DS_VA_WEAPONCARGO_BLU = [
    "arifle_MX_GL_F","arifle_MX_GL_Black_F","arifle_MX_GL_khk_F","arifle_MX_F",
    "arifle_MX_Black_F","arifle_MX_khk_F","arifle_MXC_F","arifle_MXC_Black_F",
    "arifle_MXC_khk_F","arifle_MXM_F","arifle_MXM_Black_F","arifle_MXM_khk_F",
    "arifle_SPAR_01_blk_F","arifle_SPAR_01_khk_F","arifle_SPAR_01_snd_F",
    "arifle_SPAR_01_GL_blk_F","arifle_SPAR_01_GL_khk_F","arifle_SPAR_01_GL_snd_F",
    "arifle_SPAR_02_blk_F","arifle_SPAR_02_khk_F","arifle_SPAR_02_snd_F",
    "arifle_SPAR_03_blk_F","arifle_SPAR_03_khk_F","arifle_SPAR_03_snd_F",
    "hgun_Pistol_heavy_01_F","hgun_ACPC2_F","hgun_P07_F","hgun_P07_khk_F",
    "hgun_Pistol_heavy_02_F","LMG_03_F","LMG_Mk200_F","arifle_MX_SW_F",
    "arifle_MX_SW_Black_F","arifle_MX_SW_khk_F","MMG_02_black_F","MMG_02_camo_F",
    "MMG_02_sand_F","hgun_PDW2000_F","SMG_05_F","SMG_02_F","SMG_01_F",
    "srifle_DMR_04_Tan_F","srifle_DMR_04_F","srifle_DMR_05_tan_f","srifle_DMR_05_blk_F",
    "srifle_LRR_F","srifle_LRR_camo_F","srifle_LRR_tna_F","srifle_DMR_02_F",
    "srifle_DMR_02_camo_F","srifle_DMR_02_sniper_F","srifle_DMR_03_F","srifle_DMR_03_multicam_F",
    "srifle_DMR_03_khaki_F","srifle_DMR_03_tan_F",'srifle_DMR_03_woodland_F',
    "srifle_DMR_06_camo_F","srifle_DMR_06_olive_F","srifle_EBR_F",
    "launch_NLAW_F","launch_B_Titan_F","launch_B_Titan_tna_F","launch_O_Titan_short_F",
    "launch_I_Titan_short_F","launch_B_Titan_short_F","launch_B_Titan_short_tna_F"
];

//Magazine Cargo for virtual arsenal
DS_VA_MAGAZINECARGO_BLU = [
    "30Rnd_556x45_Stanag","30Rnd_556x45_Stanag_green","30Rnd_556x45_Stanag_red",
    "30Rnd_556x45_Stanag_Tracer_Red","30Rnd_556x45_Stanag_Tracer_Green",
    "30Rnd_556x45_Stanag_Tracer_Yellow","30Rnd_65x39_caseless_mag","30Rnd_65x39_caseless_green",
    "30Rnd_65x39_caseless_mag_Tracer","30Rnd_65x39_caseless_green_mag_Tracer","20Rnd_762x51_Mag",
    "7Rnd_408_Mag","5Rnd_127x108_Mag","100Rnd_65x39_caseless_mag","100Rnd_65x39_caseless_mag_Tracer",
    "200Rnd_65x39_cased_Box","200Rnd_65x39_cased_Box_Tracer","30Rnd_9x21_Mag","30Rnd_9x21_Red_Mag",
    "30Rnd_9x21_Yellow_Mag","30Rnd_9x21_Green_Mag","16Rnd_9x21_Mag","30Rnd_9x21_Mag_SMG_02",
    "30Rnd_9x21_Mag_SMG_02_Tracer_Red","30Rnd_9x21_Mag_SMG_02_Tracer_Yellow","30Rnd_9x21_Mag_SMG_02_Tracer_Green",
    "16Rnd_9x21_red_Mag","16Rnd_9x21_green_Mag","16Rnd_9x21_yellow_Mag","NLAW_F",
    "1Rnd_HE_Grenade_shell","3Rnd_HE_Grenade_shell","1Rnd_Smoke_Grenade_shell","3Rnd_Smoke_Grenade_shell",
    "1Rnd_SmokeRed_Grenade_shell","3Rnd_SmokeRed_Grenade_shell","1Rnd_SmokeGreen_Grenade_shell",
    "3Rnd_SmokeGreen_Grenade_shell","1Rnd_SmokeYellow_Grenade_shell","3Rnd_SmokeYellow_Grenade_shell",
    "1Rnd_SmokePurple_Grenade_shell","3Rnd_SmokePurple_Grenade_shell","1Rnd_SmokeBlue_Grenade_shell",
    "3Rnd_SmokeBlue_Grenade_shell","1Rnd_SmokeOrange_Grenade_shell","3Rnd_SmokeOrange_Grenade_shell",
    "HandGrenade","MiniGrenade","SmokeShell","SmokeShellRed","SmokeShellGreen","SmokeShellYellow",
    "SmokeShellPurple","SmokeShellBlue","SmokeShellOrange","Chemlight_green","Chemlight_red",
    "Chemlight_yellow","Chemlight_blue","8Rnd_82mm_Mo_shells","8Rnd_82mm_Mo_Flare_white","8Rnd_82mm_Mo_Smoke_white",
    "8Rnd_82mm_Mo_guided","8Rnd_82mm_Mo_LG","UGL_FlareWhite_F","3Rnd_UGL_FlareWhite_F","UGL_FlareGreen_F",
    "3Rnd_UGL_FlareGreen_F","UGL_FlareRed_F","3Rnd_UGL_FlareRed_F","UGL_FlareYellow_F","3Rnd_UGL_FlareYellow_F",
    "UGL_FlareCIR_F","3Rnd_UGL_FlareCIR_F","FlareWhite_F","FlareGreen_F","FlareRed_F","FlareYellow_F",
    "Laserbatteries","30Rnd_45ACP_Mag_SMG_01","30Rnd_45ACP_Mag_SMG_01_Tracer_Green","30Rnd_45ACP_Mag_SMG_01_Tracer_Red",
    "30Rnd_45ACP_Mag_SMG_01_Tracer_Yellow","9Rnd_45ACP_Mag","150Rnd_762x51_Box","150Rnd_762x51_Box_Tracer",
    "Titan_AA","Titan_AP","Titan_AT","11Rnd_45ACP_Mag","6Rnd_45ACP_Cylinder","10Rnd_762x51_Mag",
    "10Rnd_762x54_Mag","5Rnd_127x108_APDS_Mag","B_IR_Grenade","ATMine_Range_Mag","APERSMine_Range_Mag",
    "APERSBoundingMine_Range_Mag","SLAMDirectionalMine_Wire_Mag","APERSTripMine_Wire_Mag",
    "ClaymoreDirectionalMine_Remote_Mag","SatchelCharge_Remote_Mag","DemoCharge_Remote_Mag",
    "IEDUrbanBig_Remote_Mag","IEDLandBig_Remote_Mag","IEDUrbanSmall_Remote_Mag","IEDLandSmall_Remote_Mag",
    "6Rnd_GreenSignal_F","6Rnd_RedSignal_F","10Rnd_338_Mag","130Rnd_338_Mag","10Rnd_127x54_Mag",
    "150Rnd_93x64_Mag","10Rnd_93x64_DMR_05_Mag","10Rnd_9x21_Mag","20Rnd_650x39_Cased_Mag_F","10Rnd_50BW_Mag_F",
    "150Rnd_556x45_Drum_Mag_F","150Rnd_556x45_Drum_Mag_Tracer_F","200Rnd_556x45_Box_F","200Rnd_556x45_Box_Red_F",
    "200Rnd_556x45_Box_Tracer_F","200Rnd_556x45_Box_Tracer_Red_F"
];

//Item Cargo for virtual arsenal
DS_VA_ITEMCARGO_BLU = [
    "acc_flashlight","acc_pointer_IR","B_UavTerminal","Binocular","bipod_01_F_blk","bipod_01_F_khk",
    "bipod_01_F_mtp","bipod_01_F_snd","FirstAidKit","ItemCompass","ItemGPS","ItemMap","ItemRadio",
    "ItemWatch","LaserDesignator","Laserdesignator_01_khk_F","Laserdesignator_02","Laserdesignator_02_ghex_F",
    "Laserdesignator_03","Medikit","MineDetector","muzzle_snds_338_black","muzzle_snds_338_green",
    "muzzle_snds_338_sand","muzzle_snds_58_blk_F","muzzle_snds_58_wdm_F","muzzle_snds_65_TI_blk_F",
    "muzzle_snds_65_TI_ghex_F","muzzle_snds_65_TI_hex_F","muzzle_snds_93mmg","muzzle_snds_93mmg_tan",
    "muzzle_snds_acp","muzzle_snds_B","muzzle_snds_B_khk_F","muzzle_snds_B_snd_F","muzzle_snds_H",
    "muzzle_snds_H_khk_F","muzzle_snds_H_MG","muzzle_snds_H_MG_blk_F","muzzle_snds_H_MG_khk_F",
    "muzzle_snds_H_snd_F","muzzle_snds_H_SW","muzzle_snds_L","muzzle_snds_M","muzzle_snds_m_khk_F",
    "muzzle_snds_m_snd_F","NVGoggles","NVGoggles_tna_F","NVGogglesB_blk_F","NVGogglesB_grn_F",
    "NVGogglesB_gry_F","optic_Aco","optic_ACO_grn","optic_ACO_grn_smg","optic_Aco_smg","optic_AMS",
    "optic_AMS_khk","optic_AMS_snd","optic_Arco","optic_Arco_blk_F","optic_Arco_ghex_F","optic_DMS",
    "optic_DMS_ghex_F","optic_ERCO_blk_F","optic_ERCO_khk_F","optic_ERCO_snd_F","optic_Hamr","optic_Hamr_khk_F",
    "optic_Holosight","optic_Holosight_blk_F","optic_Holosight_khk_F","optic_Holosight_smg","optic_Holosight_smg_blk_F",
    "optic_KHS_blk","optic_KHS_hex","optic_KHS_old","optic_KHS_tan","optic_LRPS","optic_LRPS_ghex_F",
    "optic_LRPS_tna_F","optic_MRCO","optic_MRD","optic_Nightstalker","optic_NVS","optic_SOS","optic_SOS_khk_F",
    "optic_tws","optic_tws_mg","optic_Yorris","Rangefinder","ToolKit","U_B_CombatUniform_mcam",
    "U_B_CombatUniform_mcam_tshirt","U_B_CombatUniform_mcam_vest","U_B_GhillieSuit","U_B_HeliPilotCoveralls",
    "U_B_Wetsuit","U_B_CombatUniform_mcam_worn","U_B_CombatUniform_wdl","U_B_CombatUniform_wdl_tshirt",
    "U_B_CombatUniform_wdl_vest","U_B_CombatUniform_sgg","U_B_CombatUniform_sgg_tshirt","U_B_CombatUniform_sgg_vest",
    "U_B_SpecopsUniform_sgg","U_B_PilotCoveralls","U_B_CTRG_1","U_B_CTRG_2","U_B_CTRG_3","U_B_FullGhillie_lsh",
    "U_B_FullGhillie_sard","U_B_FullGhillie_ard","U_B_T_Soldier_F","U_B_T_Soldier_AR_F","U_B_T_Soldier_SL_F",
    "U_B_T_Sniper_F","U_B_T_FullGhillie_tna_F","U_B_CTRG_Soldier_F","U_B_CTRG_Soldier_2_F","U_B_CTRG_Soldier_3_F",
    "U_B_CTRG_Soldier_urb_1_F","U_B_CTRG_Soldier_urb_2_F","U_B_CTRG_Soldier_urb_3_F","V_Rangemaster_belt",
    "V_BandollierB_khk","V_BandollierB_cbr","V_BandollierB_rgr","V_BandollierB_blk","V_BandollierB_oli",
    "V_PlateCarrier1_rgr","V_PlateCarrier2_rgr","V_PlateCarrier2_blk","V_PlateCarrier3_rgr","V_PlateCarrierGL_rgr",
    "V_PlateCarrierGL_blk","V_PlateCarrierGL_mtp","V_PlateCarrier1_blk","V_PlateCarrierSpec_rgr","V_PlateCarrierSpec_blk",
    "V_PlateCarrierSpec_mtp","V_Chestrig_rgr","V_Chestrig_blk","V_Chestrig_oli","V_TacVest_oli","V_TacVest_blk",
    "V_TacVest_camo","V_TacVestIR_blk","V_TacVestCamo_khk","V_PlateCarrierIAGL_oli","V_RebreatherB","V_PlateCarrierL_CTRG",
    "V_PlateCarrierH_CTRG","H_HelmetB","H_HelmetB_paint","H_HelmetB_light","H_Booniehat_khk","H_Booniehat_oli",
    "H_Booniehat_mcamo","H_Booniehat_grn","H_Booniehat_tan","H_Booniehat_dirty","H_Booniehat_khk_hs","H_HelmetB_plain_mcamo",
    "H_HelmetB_plain_blk","H_HelmetSpecB","H_HelmetSpecB_paint1","H_HelmetSpecB_paint2","H_HelmetSpecB_blk",
    "H_HelmetSpecB_snakeskin","H_HelmetSpecB_sand","H_HelmetB_grass","H_HelmetB_snakeskin","H_HelmetB_desert",
    "H_HelmetB_black","H_HelmetB_sand","H_Cap_red","H_Cap_blu","H_Cap_oli","H_Cap_headphones","H_Cap_tan",
    "H_Cap_blk","H_Cap_tan_specops_US","H_Cap_khaki_specops_UK","H_Cap_grn","H_Cap_oli_hs","H_Cap_usblack",
    "H_HelmetCrew_B","H_PilotHelmetFighter_B","H_PilotHelmetHeli_B","H_CrewHelmetHeli_B","H_MilCap_mcamo",
    "H_MilCap_oucamo","H_MilCap_blue","H_HelmetB_light_grass","H_HelmetB_light_snakeskin","H_HelmetB_light_desert",
    "H_HelmetB_light_black","H_HelmetB_light_sand","H_BandMask_blk","H_BandMask_khk","H_BandMask_reaper",
    "H_BandMask_demon","H_Bandanna_khk","H_Bandanna_cbr","H_Bandanna_sgg","H_Bandanna_sand","H_Bandanna_gry",
    "H_Bandanna_blu","H_Bandanna_camo","H_Bandanna_mcamo","H_Shemag_khk","H_Shemag_tan","H_Shemag_olive",
    "H_Shemag_olive_hs","H_ShemagOpen_khk","H_ShemagOpen_tan","H_Watchcap_blk","H_Watchcap_cbr","H_Watchcap_khk",
    "H_Watchcap_camo","H_Watchcap_sgg","H_HelmetB_tna_F","H_HelmetB_Enh_tna_F","H_HelmetB_Light_tna_F","H_MilCap_tna_F",
    "H_Booniehat_tna_F","H_Beret_gen_F","H_MilCap_gen_F","V_PlateCarrier1_tna_F","V_PlateCarrier2_tna_F","V_PlateCarrierSpec_tna_F",
    "V_PlateCarrierGL_tna_F","V_TacVest_gen_F","V_PlateCarrier1_rgr_noflag_F","G_Aviator","G_B_Diving","G_Balaclava_blk",
    "G_Balaclava_combat","G_Balaclava_lowprofile","G_Balaclava_oli","G_Balaclava_TI_blk_F","G_Balaclava_TI_G_blk_F",
    "G_Balaclava_TI_G_tna_F","G_Balaclava_TI_tna_F","G_Bandanna_aviator","G_Bandanna_beast","G_Bandanna_blk",
    "G_Bandanna_khk","G_Bandanna_oli","G_Bandanna_shades","G_Bandanna_sport","G_Bandanna_tan","G_Combat",
    "G_Combat_Goggles_tna_F","G_Diving","G_Lowprofile","G_Shades_Black","G_Shades_Blue","G_Shades_Green",
    "G_Shades_Red","G_Spectacles","G_Spectacles_Tinted","G_Sport_Blackred","G_Sport_BlackWhite","G_Sport_Blackyellow",
    "G_Sport_Checkered","G_Sport_Greenblack","G_Sport_Red","G_Squares","G_Squares_Tinted","G_Tactical_Black","G_Tactical_Clear"
];

//Backpack cargo for virtual arsenal
DS_VA_BACKPACKCARGO_BLU = [
    "B_AssaultPack_khk","B_AssaultPack_rgr","B_AssaultPack_sgg","B_AssaultPack_blk","B_AssaultPack_cbr",
    "B_AssaultPack_mcamo","B_Kitbag_rgr","B_Kitbag_mcamo","B_Kitbag_cbr","B_TacticalPack_rgr",
    "B_TacticalPack_mcamo","B_TacticalPack_blk","B_TacticalPack_oli","B_FieldPack_khk","B_FieldPack_cbr",
    "B_FieldPack_blk","B_Carryall_mcamo","B_Carryall_khk","B_Carryall_cbr","B_Bergen_sgg",
    "B_Bergen_mcamo","B_Bergen_rgr","B_Bergen_blk","B_OutdoorPack_blk","B_OutdoorPack_tan",
    "B_OutdoorPack_blu","B_AssaultPackG","B_BergenG","B_Parachute","B_AssaultPack_rgr_LAT",
    "B_AssaultPack_rgr_Medic","B_AssaultPack_rgr_Repair","B_Assault_Diver","B_AssaultPack_blk_DiverExp",
    "B_Kitbag_rgr_Exp","B_FieldPack_blk_DiverExp","B_FieldPack_oli","B_Carryall_oli","B_AssaultPack_mcamo_AT",
    "B_AssaultPack_rgr_ReconMedic","B_AssaultPack_rgr_ReconExp","B_AssaultPack_rgr_ReconLAT","B_AssaultPack_mcamo_AA",
    "B_AssaultPack_mcamo_AAR","B_AssaultPack_mcamo_Ammo","B_Kitbag_mcamo_Eng","B_Carryall_mcamo_AAA",
    "B_Carryall_mcamo_AAT","B_Mortar_01_support_F","B_Mortar_01_weapon_F","B_HMG_01_support_F",
    "B_HMG_01_support_high_F","B_HMG_01_weapon_F","B_HMG_01_A_weapon_F","B_GMG_01_weapon_F",
    "B_GMG_01_A_weapon_F","B_HMG_01_high_weapon_F","B_HMG_01_A_high_weapon_F","B_GMG_01_high_weapon_F",
    "B_GMG_01_A_high_weapon_F","B_AA_01_weapon_F","B_AT_01_weapon_F","B_B_Parachute_02_F"
];