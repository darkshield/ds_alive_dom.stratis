/***
 *    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
 *    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
 *    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
 *    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
 *    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
 *    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
 * Original Author : Wyste
 * Last Editor     : Wyste
 * Last Edited     : 09/09/2016
 * Description     : Global Variable Definition / Control
 */

//------------------- Debug
DS_DEBUG              = false;
DS_DEBUG_DETAILED     = false;
DS_DEBUG_FRAMETICKS   = false;
DS_DEBUG_PARAMDUMP    = false;
DS_DEBUG_HINT         = false;
DS_DEBUG_INVUL        = true;
DS_DEBUG_TP           = false;
DS_DEBUG_SIDEMISSIONS = false;

//------------------- Client Detection
DS_A3               = true;
DS_SRV              = if(isServer)then{true}else{false};
DS_DEDICATED        = if(isDedicated)then{true}else{false};
DS_CLI              = if(!isDedicated && hasInterface)then{true}else{false};

//------------------- Forces Configuration
DS_ENEMY_SIDE       = EAST;

//------------------- Domination Configuration
DS_CAPPED_TILES     = 0;
DS_SIDEMISSIONS_ACTIVE_COUNT = 0;

//------------------- Header Includes
#include "rewards.hpp";
#include "intel.hpp";
#include "cfg_functions.hpp";
#include "equipment.hpp";

//------------------- Location Definitions
DS_LGCITY    = "NameCityCapital";
DS_CITY      = "NameCity";
DS_SMCITY    = "FlatAreaCitySmall";
DS_HILL      = "Hill";
DS_LOCAL     = "NameLocal"; //Includes Airfields and weird bases?
DS_VILLAGE   = "NameVillage";
DS_WATER     = "NameMarine";

//------------------- Blacklist Arrays
DS_OPCOM_MARKERS_USED = []; publicVariable "DS_OPCOM_MARKERS_USED";
DS_BLDGPOS_USED       = []; publicVariable "DS_BLDGPOS_USED";

//------------------- Random ClassName Arrays
DS_VANILLA_ROADMINE_OBJS = ["ATMine","SLAMDirectionalMine"]; publicVariable "AIO_VANILLA_MINE_OBJS";
DS_VANILLA_ROADMINE_PLACED = ["ATMine_Range_Ammo","SLAMDirectionalMine_Wire_Ammo"]; publicVariable "DS_VANILLA_ROADMINE_PLACED";

DS_SM_HELICRASHVEH = ["B_Heli_Attack_01_F","B_Heli_Light_01_armed_F","B_Heli_Transport_01_camo_F","B_Heli_Light_01_F"];

DS_CAS_COUNT  = 1;
DS_ARTY_COUNT = 1;

DS_BLUFOR_MARKER = "BLUFOR_1";

DS_GFP_BLU = 0; /* Friendly Forces */ publicVariable "DS_FGP_BLU";
DS_GFP_RED = 0; /* Enemy Forces    */ publicVariable "DS_FGP_RED";