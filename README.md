![DS Logo](http://darkshield.org/wp-content/uploads/2016/09/DarkShieldEmblemArma31-150x150.png) 
# Operation DarkShield - a CO-50 Domination Gamemode (ALiVE + CBA Required)
![Version 1.0](https://img.shields.io/badge/Version-1.0-blue.svg?style=flat) ![BitBucket Issues](https://img.shields.io/bitbucket/issues/darkshield/ds_alive_dom.stratis.svg?style=flat&label=Issues) 

### What is DarkShield Domination? ###

* Our version of Domination blends a domination game mode, with ALiVE's unpredicability, side missions, intel collection, and a vehicle reward system.
* Currently in heavy development, expected release is the end of September, 2016.

### Features ###

* 50x50m grid capture system (Traditional Domination)
* Found Intelligence drops / caches can be looted for side missions or nearby enemy locations.
* ALiVE mod with subtle pushes to the in-game ALiVE API by the developers to keep you on your toes.

### Would you like to contribute? Who do I talk to? ###

* Contact Wyste or BBrown via [Discord](https://discordapp.com/invite/NfccX?utm_source=Discord%20Widget&utm_medium=Connect) or via our [Website](http://www.darkshield.org)