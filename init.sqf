/***
 *    ██████╗  █████╗ ██████╗ ██╗  ██╗    ███████╗██╗  ██╗██╗███████╗██╗     ██████╗
 *    ██╔══██╗██╔══██╗██╔══██╗██║ ██╔╝    ██╔════╝██║  ██║██║██╔════╝██║     ██╔══██╗
 *    ██║  ██║███████║██████╔╝█████╔╝     ███████╗███████║██║█████╗  ██║     ██║  ██║
 *    ██║  ██║██╔══██║██╔══██╗██╔═██╗     ╚════██║██╔══██║██║██╔══╝  ██║     ██║  ██║
 *    ██████╔╝██║  ██║██║  ██║██║  ██╗    ███████║██║  ██║██║███████╗███████╗██████╔╝
 *    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═╝╚═╝╚══════╝╚══════╝╚═════╝
 * Original Author : Wyste
 * Last Editor     : Wyste
 * Last Edited     : 09/11/2016
 * Description     : Essential Arma3 Script
 */
 [] call DS_fnc_coreSetup; //Grabs parameters as well
 call compile preprocessFile "data\common_defines.sqf";
 [] call DS_fnc_debugSetup;
 [] call DS_fnc_loadoutSetup;
 [] call DS_fnc_coreIntro;

 [] spawn DS_fnc_HeartBeatSquareCheck; // 60 second heartbeat check on server.
 [] spawn DS_fnc_HeartbeatSideMissionCheck; // 60 second 60 second heartbeat check on server.